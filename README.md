# PBA Workspace
This workspace is created as a combination of the original Kobot workspace repositories and the work on PBA (Pheromone-Based Aggregation) scripts to provide testing 
capabilities to any user.

## Setup and general information
### Contact information
Maintainer: Atakan Botasun [mail](atakanbotasun@outlook.com)

### Requirements
- ROS1 Noetic (migration to ROS2 Humble is underway)

### Dotfile notes
If `tmuxinator` is installed, some launch sequences can be automated using the scripts in `dotfiles`.  
Call such scripts by the command `tmuxinator start -p dotfiles/<script-name>` at the top level.  

### Setup
- Install ROS Noetic according to the instructions found in [wiki.ros.org](wiki.ros.org).
- Run the following line at the top level to install ROS package dependencies.
```sh
rosdep install --from-paths ./src --ignore-packages-from-source --rosdistro noetic -y
```
- When necessary packages are installed from rosdep, packages found in this workspace can be built.
```sh
catkin_make
```

## Instructions
Detailed instructions on how to use each package can be found in their corresponding `README.md` files.