# Aggregation package
## Description
This package is meant for any aggregation algorithm and necessary utilities running on the Kobot platform.
## Package structure
### config
Any configuration file pertaining to the workspace/simulation setup, the node programming, or behavior parameters can be found in this folder.
- `beeclust.yml`: BEECLUST parameters, and node setup.
- `pba.yml`: PBA parameters, and node setup.
- `pheromone.yml`: Pheromone driver parameters, node setup and robot configuration details. Read the file for more information.
- `rviz_visualizer_pheromone.yml`: RViz frontend configuration for simulations and real robot tests, particularly for tests with pheromone utilities used.
- `rviz_visualizer_plain.yml`: RViz frontend configuration for simulations and real robot tests. Use this configuration if you aren't using pheromones.
- `sim_params_aggregation.yml`: Main configuration file for simulated experiments.
- `tf_config.yml`: Frame information, TF broadcaster settings for robots.
### dotfiles
These Python scripts are used as intermediate backend launchers (ROS2-like) to spawn multiple robots with associated namespaces and nodes.
### launch
XML launchfiles can be found here. Use `<algorithm-name>_start.launch` files to spawn multiple robots and set up the simulation clock and server. `<algorithm-name>.launch` files refer to the launch sequence for any real or simulated robot's behavior, once their necessary subsystems are ready.  
Other launchfiles are explained below:
- `pheromone_driver.launch`: Launch the pheromone driver to provide stigmergic communication to the robots.
- `rviz_visualizer.yml`: Launch the RViz frontend associated with the Kobot simulator.
- `teleop.launch`: Take control of a robot with the given namespace, using the topic `key_vel`. Typically for debugging purposes only.
### src
Source files for the behavior nodes and associated utilities can be found here.
#### scripts
Behavior scripts are located here.
#### testing
Tools to gather data for performance metrics can be found here.
#### utils
Additional code and data required to run the behavior scripts can be found here.
## Usage
- Set up your simulation parameters in `sim_params_aggregation.yml` and `pheromone.yml`(if pheromones are used).
- Set up visualization parameters in `rviz_visualizer_<...>.yml` according to your experimental setup.
- Run the experiment. Example experimental procedures can be found in tmuxinator `.yml` macros provided in the `dotfiles` folder **in the top level**.