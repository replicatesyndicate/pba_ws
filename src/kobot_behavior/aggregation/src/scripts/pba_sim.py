#!/usr/bin/env python3
# Standard library imports
import random
from copy import deepcopy
# External library imports
import numpy as np
# ROS imports
import rospy
from geometry_msgs.msg import Twist, Pose2D
from std_msgs.msg import Bool, Float64
from nav_msgs.msg import Odometry
from kobot_msgs.msg import range_n_bearing_sensor, floor_sensor, pheromone_sensor
# ROS-TF2 imports
import tf_conversions as tfc

class NodeHandle(object):
    def __init__(self):
        # Get Kobot namespace information
        self.kobot_namespace=rospy.get_namespace().strip("/")

        odom_freq = 20
        if rospy.has_param('odom_freq'):
            odom_freq= rospy.get_param('odom_freq')
        self.odom_rate= rospy.Rate(odom_freq)

        self.main_freq = 20
        if rospy.has_param('pba_freq'):
            self.main_freq = rospy.get_param('pba_freq')
        self.rate = rospy.Rate(self.main_freq)

        # Get parameters from ROS parameter server
        self.get_params()
        ## Set diminishing rate for pheromone emissions
        self.diminishing_rate = 1 - (1 - self.pheromone_emission_decay)**(1/self.main_freq)

        # Attribute initialization
        ## Timing information
        self.iters = 0
        self.tracking_init_time = None # = rospy.Time.now() when starting to track pheromone, attention span during pheromone tracking
        self.injection_time = None # = rospy.Time.now() after rospy.init_node

        ## Logical flags
        self.obs_detected = False
        self.robot_detected = False
        self.going_cue = False
        self.navigate_disabled = False # previously called sub_lock - disables self.navigate()
        self.attention_exceeded = False

        ## Sensor attributes
        self.robot_pose = Pose2D()
        self.I_c = 0
        self.I_avg_prev = 0
        self.range_prev = [0]*self.rb_sensors #12 sensors, works with any multiple of 4
        self.is_robot_prev = [0]*self.rb_sensors

        self.pheromone_readouts = np.zeros((3,3)).astype(np.float64)
        self.injection_value = 0.0

        ## PBA variables
        ### Pheromone tracker error terms
        self.prev_tracking_error = 0.0 # D control
        self.sum_tracking_error = 0.0 # I control

        # Publishers and Subscribers
        self.nav_vel_pub = rospy.Publisher(
            "nav_vel",
            Twist, queue_size=1)

        ## Select velocity or position control
        self.move_lock_pub = rospy.Publisher(
            "move_lock",
            Bool, queue_size=1)
        
        self.pheromone_emitter_pub = rospy.Publisher("pheromone_emitter",
                        Float64,
                        queue_size=1)

        rospy.Subscriber("sensors/range_n_bearing",
                         range_n_bearing_sensor,
                         self.rb_callback,
                         queue_size=1)

        rospy.Subscriber("sensors/floor",
                         floor_sensor,
                         self.intensity_callback,
                         queue_size=1)

        ## Subscribe to given odometry topic
        if self.odom_topic == "odom":
            rospy.Subscriber('odom',
                            Odometry,
                            self.odom_callback, queue_size=1)
        elif self.odom_topic == "wheel_odom":
            rospy.Subscriber('wheel_odom',
                            Odometry,
                            self.odom_callback, queue_size=1)
        else:
            rospy.logwarn("Invalid pose_select parameter. Defaulting to odom.")
            rospy.Subscriber('odom',
                            Odometry,
                            self.odom_callback, queue_size=1)
        
        rospy.Subscriber("sensors/pheromone",
                         pheromone_sensor,
                         self.pheromone_callback,
                         queue_size=1)

    # Methods
    ## Parameter access
    def get_params(self):
        """
        PBA parameters are checked for every loop and
        updated if necessary
        """
        if rospy.has_param('debug_mode'):
            self.navigator_debug_mode = rospy.get_param('debug_mode')
        else:
            self.navigator_debug_mode = False
            
        if rospy.has_param('pba_params'):
            # fetch a group (dictionary) of parameters
            params = rospy.get_param('pba_params')
            if self.navigator_debug_mode:
                self.cue_wait_max = 20
            else:    
                self.cue_wait_max = params['cue_wait_max']
            self.u_max = params['u_max']
            self.w_max = params['w_max']
            self.u_roam = params['u_roam']
            self.min_angular_err = params['min_ang_err']
            self.max_angular_err = params['max_ang_err']
            self.K_p = params['K_p']
            self.zeta_range = params['zeta_range']
            self.zeta_is_robot = params['zeta_is_robot']
            self.obs_detection_thresh_1 = params['obs_detection_thresh_1']
            self.obs_detection_thresh_2 = params['obs_detection_thresh_2']
            self.obs_detection_thresh_3 = params['obs_detection_thresh_3']
            self.cue_exit_robot_thresh_1 = params['cue_exit_robot_thresh_1']
            self.cue_exit_robot_thresh_2 = params['cue_exit_robot_thresh_2']
            self.obs_robot_detection_thresh_1 = params['obs_robot_detection_thresh_1']
            self.obs_robot_detection_thresh_2 = params['obs_robot_detection_thresh_2']
            self.obs_robot_detection_thresh_3 = params['obs_robot_detection_thresh_3']
            self.robot_detection_thresh = params['robot_detection_thresh']
            self.avoid_thresh = params['avoid_thresh']
            self.avoid_failure_thresh = params['avoid_failure_thresh']
            self.I_thresh = params["I_thresh"]
            self.I_const = params["I_const"]
            self.max_rand_ang = params["max_rand_ang"]
            self.odom_topic = params["odom_topic"] # either "odom" or "wheel_odom"
            self.rb_sensors = params["rb_sensors"] # works with any multiple of 4
            self.turn_theta_max_iters = params["turn_theta_max_iters"] # divide by odom_rate to find duration in seconds
            self.pheromone_threshold = params["pheromone_threshold"]
            self.gradient_kernel_one = params["gradient_kernel_one"]
            self.gradient_kernel_two = params["gradient_kernel_two"]
            self.pheromone_emission_intensity = params["pheromone_emission_intensity"]
            self.cue_to_pheromone_modulation = params['cue_to_pheromone_modulation']
            self.pheromone_emission_decay = params["pheromone_emission_decay"]
            self.pheromone_emission_duration = params["pheromone_emission_duration"]
            self.pheromone_readmission_time = params["pheromone_readmission_time"]
            self.gradient_tracker = params['gradient_tracker']
            self.pheromone_K_p = params['pheromone_K_p']
            self.pheromone_K_i = params['pheromone_K_i']
            self.pheromone_K_d = params['pheromone_K_d']
            self.speed_modulation = params['speed_modulation']
            self.tracking_velocity = params['tracking_velocity']
            self.attention_threshold = params['attention_threshold']
            # self.tracking_limit = params['tracking_limit']
            self.dynamic_pba_params = params['dynamic_pba_params']
        else:  # feed default vals
            rospy.logwarn_once("[{}/pba.py]: Parameters are assigned default values!".format(self.kobot_namespace))
            self.cue_wait_max = 90
            self.u_max = 0.12
            self.w_max = 0.8
            self.u_roam = 0.05
            self.min_angular_err = 0.1
            self.max_angular_err = 2.60
            self.K_p = 0.87
            self.zeta_range = 1.0
            self.zeta_is_robot = 1.0
            self.obs_detection_thresh_1 = 165
            self.obs_detection_thresh_2 = 155
            self.obs_detection_thresh_3 = 155
            self.obs_robot_detection_thresh_1 = 165
            self.obs_robot_detection_thresh_2 = 155
            self.obs_robot_detection_thresh_3 = 155
            self.cue_exit_robot_thresh_1 = 165
            self.cue_exit_robot_thresh_2 = 145
            self.robot_detection_thresh = 165
            self.avoid_thresh = 165
            self.avoid_failure_thresh = 165
            self.I_thresh = 150
            self.I_const = 1000
            self.max_rand_ang = 0.0
            self.odom_topic = "odom" # either "odom" or "wheel_odom"
            self.rb_sensors = 12 # works with any multiple of 4
            self.turn_theta_max_iters = 420 # divide by odom_rate to find duration in seconds
            self.pheromone_threshold = 10.0
            self.gradient_kernel_one = [1.,0.,-1] # defaults to Sobel operator kernels
            self.gradient_kernel_two = [1.,2.,1.] 
            self.pheromone_emission_intensity = 20.0
            self.cue_to_pheromone_modulation = 0.8
            self.pheromone_emission_decay = 0.1
            self.pheromone_emission_duration = 10.0
            self.pheromone_readmission_time = 5.0
            self.gradient_tracker = "weighted"
            self.pheromone_K_p = 0.250
            self.pheromone_K_i = 0.000
            self.pheromone_K_d = -0.065
            self.speed_modulation = 0.9
            self.tracking_velocity = 0.03
            self.attention_threshold = 100.0
            # self.tracking_limit = 3
            self.dynamic_pba_params = True # fetch parameters later if they are not currently available
        
        if (self.rb_sensors % 4) != 0:
            # range and bearing subsystem must have a multiple of 4 sensor inputs.
            # if this value cannot be asserted, use the default value indicated above
            self.rb_sensors = 12

        self.frontal_rb_list = [i for i in range(self.rb_sensors) if 3*self.rb_sensors/4<i<self.rb_sensors or 0<i<self.rb_sensors/4]
        self.side_rb_list = [int(self.rb_sensors/4), int(3*self.rb_sensors/4)]
        # image gradient kernels
        self.k1 = np.array(self.gradient_kernel_one)
        self.k2 = np.array(self.gradient_kernel_two)    
        # pheromone injection diminishing rate
        self.emission_diminishing_rate = 1 - (1 - self.pheromone_emission_decay)**(1/20)
        
    ## Callback methods
    def rb_callback(self, data):
        """
        Range values from the range and bearing
        """
        # preallocate flags before processing sensor data
        self.obs_detected = False
        self.robot_detected = False
        self.rb = range_n_bearing_sensor()
        self.rb = data.range_n_bearing

        range_prev = deepcopy(self.range_prev)
        is_robot_prev = deepcopy(self.is_robot_prev)
        self.range_prev = [0]*self.rb_sensors
        self.is_robot_prev = [0]*self.rb_sensors

        self.detected_sensor_angs = []

        for indx, sensor_reading in enumerate(data.range_n_bearing):

            # filter range values and update prev filtered lists
            range_val = moving_average_filter(sensor_reading.range, range_prev[indx], self.zeta_range)
            self.range_prev[indx] = range_val
            # filter is_robot vals.
            is_robot = moving_average_filter(sensor_reading.is_robot, is_robot_prev[indx], self.zeta_is_robot)
            self.is_robot_prev[indx] = is_robot
            # range_val = int(range_val)
            if is_robot < 0.5:
                is_robot = False
            else:
                is_robot = True

            if not is_robot:
                if range_val < self.obs_detection_thresh_1:
                    if indx in [0]:
                        # 0 is the front facing sensor of the robot
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_1, indx)
                    elif indx in self.frontal_rb_list:
                        # 1, 7  front left and right facing sensors for 8 sensor setup
                        # 1, 2, 10, 11 front left and right facing sensors for 12 sensor setup
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_2, indx)

                    elif indx in self.side_rb_list:
                        # 2, 6 left and right facing sensors for 8 sensor setup
                        # 3, 9 left and right facing sensors for 12 sensor setup
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_3, indx)

            else:
                if range_val < self.robot_detection_thresh:
                    # all sensors used for robot detection
                    self.robot_detected = True
                    # robots in extremely close proximity are treated as obstacles
                    if indx in [0]:
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_1, indx)
                    elif indx in self.frontal_rb_list:
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_2, indx)
                    elif indx in self.side_rb_list:
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_3, indx)
            if self.detected_sensor_angs != []:
                self.detected_sensor_ang = sum(self.detected_sensor_angs) / float(len(self.detected_sensor_angs))

    def intensity_callback(self, data):
        """
        Intensity vals. from the floor sensors
        """
        I_list = data.intensity
        # I_list = [ord(intensity) for intensity in data.intensity] # use this if floor_sensor() msg has non-standard int typing

        I_sum = sum(I_list)
        I_avg = I_sum / len(I_list)
        self.I_c = I_avg

    def odom_callback(self, data):
        """
        Odom callback for 2-D robot pose
        """
        quaternion = data.pose.pose.orientation
        explicit_quat = [quaternion.x, quaternion.y, quaternion.z, quaternion.w]
        _, _, yaw = tfc.transformations.euler_from_quaternion(explicit_quat)
        x, y = data.pose.pose.position.x, data.pose.pose.position.y
        theta = yaw
        self.robot_pose = Pose2D(x=x, y=y, theta=theta)

    def pheromone_callback(self,data):
        """
        Pheromone callback, either with virtualized pheromones or real pheromones.
        Requires an external sensor plugin to feed the callback values.
        Inputs:
        - Data (std_msgs/Float64MultiArray): Intensity readout
        """
        # float64 conversion may not be necessary, but done just to be safe
        self.pheromone_readouts = np.asarray(data.readouts).reshape((3,3)).astype(np.float64)

    ## Helper methods
    def obs_detection(self, range_val, range_thresh, indx):
        """
        Decide if an obstacle is detected based on the inputs
        """
        if range_val < range_thresh:
            self.obs_detected = True
            self.detected_sensor_angs.append(wrap2pi(indx * 2*np.pi/self.rb_sensors))

    def avoid_nicely(self):
        """
        How the robot avoids objects randomly in BEECLUST and its variants.
        The random action must be within a space where the robot can successfully avoid its obstacles.
        If such a random action does not exist, the robot should turn in a random direction until it is freed from obstacles.
        """
        theta = random.uniform(+np.pi/2 + self.max_rand_ang,
            3*np.pi/2 - self.max_rand_ang)
        theta += self.detected_sensor_ang
        theta = wrap2pi(theta)
        # find corresponding rb after rotation
        decision_indx = int(theta / (2*np.pi/self.rb_sensors))
        if self.range_prev[decision_indx] > self.avoid_thresh and self.range_prev[(decision_indx+1)%self.rb_sensors] > self.avoid_thresh:
            # if random decision gets us out follow it
            self.turn_theta(theta, self.odom_rate)
            self.publish_twist(self.u_roam, 0)
        else:
            # random walk can't get robot into the clear, turn until front is cleared
            rate = rospy.Rate(20)
            is_ccw = random.choice([True, False])
            while True:
                if self.range_prev[0] > self.avoid_failure_thresh:
                    self.publish_twist(self.u_roam, 0)
                    break
                else:
                    if is_ccw:
                        self.publish_twist(0, 1.2) 
                    else:
                        self.publish_twist(0, -1.2)
                rate.sleep()

    def sleep_w_s(self, I_c):
        """
        Compute wait time from the cue intensity reading, then wait by that amount
        """
        # stop and rotate
        self.publish_twist(0, 0)
        float_I = float(I_c)
        # compute w_s
        w_s = self.cue_wait_max*(float_I**2/(float_I**2 + self.I_const))
        # mark the start time
        start_time = rospy.Time.now()
        rate = rospy.Rate(1)
        iters = 0
        while not rospy.is_shutdown():
            # calculate time passed in secs
            time_passed = rospy.Time.now() - start_time
            time_passed = time_passed.secs
            if iters % 75 == 0:
                rospy.loginfo("[{}]: waiting on cue for {:.2f} sec".format(
                    self.kobot_namespace,w_s-time_passed))
            if w_s - time_passed < 0:
                # time is up
                break
            rate.sleep()
            #add a tick to the timer
            iters += 1

    def exit_cue_rand(self):
        """
        Tries to exit from cue by rotating randomly
        and if front sensors doesn't detect any robot
        tries to get away from the cue for 5s unless
        any other robot or obstacle seen in front
        """
        # apply random turn action
        theta = random.uniform(-np.pi, np.pi)
        self.turn_theta(theta, self.odom_rate)
        # go straight for 6 seconds to leave cue area
        rate = rospy.Rate(20)
        # each loop takes 0.05 s
        for _ in range(120):
            if rospy.is_shutdown():
                return
            self.publish_twist(self.u_roam, 0)
            # check whether exit path from cue is occluded by another robot
            if self.rb[0].is_robot and self.rb[0].range < self.cue_exit_robot_thresh_1:
                self.publish_twist(0, 0)
                # self.injection_value = 0.0
                try:
                    self.injection_time = rospy.Time.now() - rospy.Duration(self.pheromone_emission_duration)
                except:
                    self.injection_time = rospy.Time()
                return
            for indx in self.frontal_rb_list:
                if self.rb[indx].is_robot and self.rb[indx].range < self.cue_exit_robot_thresh_2:
                    self.publish_twist(0, 0)
                    # self.injection_value = 0.0
                    try:
                        self.injection_time = rospy.Time.now() - rospy.Duration(self.pheromone_emission_duration)
                    except:
                        self.injection_time = rospy.Time()
                    return
            if self.obs_detected:
                self.publish_twist(0, 0)
                # self.injection_value = 0.0
                try:
                    self.injection_time = rospy.Time.now() - rospy.Duration(self.pheromone_emission_duration)
                except:
                    self.injection_time = rospy.Time()
                return
            self.inject(self.emission_diminishing_rate) # inject while moving
            rate.sleep()

    def turn_theta(self, theta_rel, rate):
        """
        Turns by theta relative to the current 
        orientation in a closed loop manner 
        from the feedback of the odometry callback
        
        Inputs:
        - theta_rel [float]: Relative angular displacement to turn to
        - rate [rospy.Rate]: Rate to turn at. If a numerical value is provided instead, conversion is handled in function. 
        """
        if type(rate) != rospy.timer.Rate:
            try:
                rate = rospy.Rate(rate)
            except Exception as e:
                rospy.logerr(e)
        
        self.publish_move_lock(True)
        self.navigate_disabled = True
        # desired angle is current angle + rel angle
        theta_des = self.robot_pose.theta + theta_rel
        # initialize timer ticks for screen outputs and break conditions
        iters = 0
        self.publish_twist(0,0) # ensure that the robot is stationary before turning action
        # angle control loop should be blocking
        while not rospy.is_shutdown():
            # error is the difference among current angle and desired angle, limited to -pi, +pi
            angle_err = wrap2pi(theta_des - self.robot_pose.theta)
            if iters % 40 == 0 and self.going_cue:
                rospy.logdebug("[{}]: Current angular error: {}".format(self.kobot_namespace,angle_err*np.pi/180))

            if abs(angle_err) <= self.min_angular_err:
                if self.going_cue:
                    rospy.loginfo("Turn action completed! Switching back to the main loop.")
                self.publish_twist(0,0) # ensure that the robot applies the subsequent twist command by stopping first
                # reached the target angle
                break
            omega = angle_err * self.K_p
            # control action
            self.publish_twist(0, omega)
            rate.sleep()
            iters += 1

            if iters == self.turn_theta_max_iters:
                rospy.logwarn("[{}/pba.py]: turn_theta cannot settle. Breaking.".format(self.kobot_namespace))
                break
        # stop
        self.navigate_disabled = False
    
    def pheromone_detected(self):
        """
        Checks if the pheromone readouts from the pheromone sensor is above the threshold at any given cell.
        """

        if np.any(self.pheromone_readouts > self.pheromone_threshold):
            return True
        else:
            return False
        
    def pheromone_injecting(self):
        """
        Checks if pheromones are currently being injected by the robot, by checking the relevant timestamps provided.
        """
        if self.injection_time.to_sec() != 0.0:
            time = (rospy.Time.now() - self.injection_time).to_sec()
            return time < self.pheromone_emission_duration, time
        else:
            # rospy.logwarn_once("No injection requests yet")
            return False, self.pheromone_emission_duration

    def tracking_attention_exceeded(self):
        """
        Check if any attention exceedings have occurred during pheromone tracking.
        """
        return (rospy.Time.now() - self.tracking_init_time).to_sec() >= self.attention_threshold # duration based approach

    def pheromone_tracker(self):
        """
        Tracks pheromones according to the intensity readouts from the pheromone sensor.
        Yields a direction (in radians) to move towards, which is most likely to be towards the cue area.
        Then an angular speed value is generated via PD control.
        """
        pgrid = self.pheromone_readouts.copy()

        # reorient to the front of the robot and take reciprocal to investigate ascending gradient
        sgrid = np.rot90(pgrid, k=-1)

        # prepare image gradient filter kernels
        if self.gradient_tracker == "convolution":
            # Apply image gradient filter to get gradient values in X and Y directions
            gradient_x = decomposed_conv2d(sgrid,self.k2,self.k1)
            gradient_y = decomposed_conv2d(sgrid,self.k1,self.k2)

            directions = np.arctan2(gradient_y,gradient_x)       
            # theta_rel = wrap2pi(np.sum(directions))
            theta_rel = wrap2pi(-directions[1,1])
        
        # use a heading scale
        if self.gradient_tracker == "weighted":
            sgrid[1,1] = -1 # not using the center
            srt = np.sort(sgrid, axis=None)[-2:] # take two largest items
            vals = np.where(np.isin(sgrid,srt), sgrid, 0.0)
            angs = np.array([
                [ 3*np.pi/2,  np.pi/2,         np.pi/4 ],
                [ np.pi,          0.,          0.      ],
                [-3*np.pi/2, -np.pi/2,        -np.pi/4 ]
                ])
            theta_rel = wrap2pi(np.sum(vals*angs/np.sum(vals)/2))

        if self.iters % 50 == 0:
            rospy.logdebug("Angle offset: {:.2f} deg".format(np.rad2deg(theta_rel)))
            # rospy.loginfo("Pheromone:\n{}\n{}\n{}".format(pgrid[0],pgrid[1],pgrid[2]))
            # rospy.loginfo("Shapes: last:{}, theta:{}, thres:{}".format(self.last_direction.shape,theta_des.shape,self.pheromone_turn_threshold.shape))

        # error is the difference among current angle and desired angle, limited to -pi, +pi
        angle_err = wrap2pi(theta_rel)
        # rospy.logwarn("[{}: Error: {}]".format(self.kobot_namespace, angle_err))
        if abs(angle_err) <= self.min_angular_err:
            angular_speed = 0.0
            self.prev_tracking_error = 0.0
            self.sum_tracking_error = 0.0
        elif abs(angle_err) <= self.max_angular_err:
            angular_speed_val = self.pheromone_K_p * angle_err + self.pheromone_K_d * (angle_err - self.prev_tracking_error) / self.main_freq + self.pheromone_K_i * self.sum_tracking_error
            angular_speed_sgn = np.sign(angular_speed_val)
            angular_speed_mag = min(abs(angular_speed_val),self.w_max)
            angular_speed = angular_speed_sgn * angular_speed_mag

            self.prev_tracking_error = angle_err
            self.sum_tracking_error += angle_err
            rospy.logdebug("Angular speed: {}".format(angular_speed))
        else:
            angular_speed = 0.0
        if self.iters % 80 == 0:
            rospy.loginfo("Angular speed: {}".format(angular_speed))

        err_mod = (np.pi - self.speed_modulation*abs(angle_err))/np.pi 
        speed = self.tracking_velocity * err_mod  

        self.publish_twist(speed,angular_speed)
        # self.odom_rate.sleep() # if sleeping here, make sure to add the odom_freq to self.main_freq in expression above

    def publish_move_lock(self, bool_val):
        """
        Send state of the lock to the pose_controller
        when state is true velocity controller takes on the command
        when state is false position controller takes on the command
        """
        move_lock_msg = Bool()
        move_lock_msg = bool_val
        self.move_lock_pub.publish(move_lock_msg)

    def publish_twist(self, x_vel, theta_vel):
        """
        Publish ref. vals. for vel. controller
        """
        twist_msg = Twist()
        twist_msg.linear.x = x_vel
        twist_msg.angular.z = theta_vel

        self.nav_vel_pub.publish(twist_msg)
    
    # Primary method definitions
    def inject(self, diminishing_rate):
        """
        Inject pheromones, as long as the duration to inject pheromones is not exceeded.
        """
        try:
            # rospy.loginfo("Remaining injection time: {}".format((self.injection_time - rospy.Duration(self.pheromone_emission_duration)).to_sec()))
            # rospy.loginfo("Injection value: {}". format(self.injection_value))
            injecting, remaining_time = self.pheromone_injecting()
            if not injecting:
                if self.injection_value != 0.0:
                    rospy.logdebug("[{}]: Ceased injections (remaining time: {})".format(self.kobot_namespace, remaining_time))
                self.injection_value = 0.0
            if self.iters % 200 == 0:
                rospy.logdebug("[{}]: Injection value: {}".format(self.kobot_namespace,self.injection_value))
            self.pheromone_emitter_pub.publish(self.injection_value)
            self.injection_value *= (1 - diminishing_rate)
        except TypeError: # positive values only error in the beginning - will fix itself after a short time
            pass

    def navigate(self):
        """
        Main navigator function to use with aggregation algorithms.\n
        Called once every loop. Executes the finite state machine for the robot.\n
        ---------------------------------------------------------------------------

        PBA behavior:

        \tRandom walk unless in cue area, or a pheromone trail.\n
        \tTracking a pheromone trail will disable random walk.\n  
        \tLeaving the cue area will trigger pheromone injection.\n  
        \tEncountering other agents in cue will make the robot wait for some time.\n  
        \tIf the robot encounters another during pheromone tracking, it will execute avoidance.\n
        """
        if self.navigate_disabled:
            # only triggered by turn_theta()
            if self.iters % 80 == 0:
                rospy.loginfo("[{}/pba.py]: Navigation is disabled!".format(self.kobot_namespace))
            # ignore any new messages, still processing the latest
            return
        
        injecting, _ = self.pheromone_injecting()

        try:
            if self.pheromone_detected() and self.I_c < self.I_thresh and not injecting:
                if self.going_cue == False: # just found this trail
                    self.tracking_init_time = rospy.Time.now()
                if self.iters % 50 == 0:
                    rospy.logdebug("[{}/pba.py]: Tracking scent!".format(self.kobot_namespace))
                    pass
                # the robot is navigating to the cue area
                self.going_cue = True
                
                if not self.tracking_attention_exceeded():
                    self.pheromone_tracker()
                else:
                    if not self.attention_exceeded:
                        rospy.logwarn("[{}/pba.py]: Attention span exceeded.".format(self.kobot_namespace))
                    self.attention_exceeded = True
            else: # lost the trail
                self.going_cue = False
                self.prev_tracking_error = 0.0 # reset D controller
                self.sum_tracking_error = 0.0 # reset I controller
        except TypeError: # not expecting other than debug scenarios (time must be positive) - already mostly mitigated
            pass

        if self.obs_detected:
            if self.I_c < self.I_thresh:
                self.publish_twist(0, 0)
                self.avoid_nicely()
            else:
                self.robot_detected = True

        if self.robot_detected or self.navigator_debug_mode:
            if self.I_c > self.I_thresh:
                rospy.loginfo("[{}/pba.py]: move_lock engaged and going_cue disabled - encountered a robot in cue area.".format(self.kobot_namespace))
                self.publish_move_lock(True)
                # clear out pheromone emissions
                # self.injection_value = 0.0
                try:
                    self.injection_time = rospy.Time.now() - rospy.Duration(self.pheromone_emission_duration)
                except:
                    self.injection_time = rospy.Time()
                self.inject(self.diminishing_rate) # inject early so that this action is not blocked by nesting
                self.going_cue = False # already in cue area
                self.attention_exceeded = False # cannot be attending to pheromones in cue area
                self.publish_twist(0, 0)
                # we are in the cue area
                self.sleep_w_s(self.I_c) # disabled in navigator debug mode
                # self.waited_in_cue = True # needed for trail reinforcement checks
                rospy.logwarn("[{}/pba.py]: Leaving cue area.".format(self.kobot_namespace))
                # self.injection_value = self.pheromone_emission_intensity
                # specify cue_to_pheromone_modulation as 0 if you don't want to modulate injections
                cue_mod = 1 + (self.I_c / 255 - 1) * self.cue_to_pheromone_modulation
                self.injection_value =  self.pheromone_emission_intensity*cue_mod
                self.injection_time = rospy.Time.now() # initialize as duration, then use time
                # slept enough now try to exit from cue (injection handled internally)
                # self.reinforcing_trail = False
                self.exit_cue_rand() 
        
        if not self.obs_detected and not self.robot_detected and (not self.going_cue or self.attention_exceeded):
            # roaming phase - no detection from the frontal range-and-bearing sensors
            # rospy.logwarn("Roaming!")
            self.publish_twist(self.u_roam, 0)          
        
        if not self.pheromone_detected() and self.attention_exceeded:
            rospy.logwarn("[{}/pba.py]: Attention span reset!".format(self.kobot_namespace))
            self.attention_exceeded = False

def moving_average_filter(val, filtered_val_prev, zeta):
    """
    Basic moving average filter
    zeta = 1 -> ignore prev. vals
    zeta = 0 -> ignore current val
    """
    filtered_val = (1-zeta)*filtered_val_prev + zeta*val
    return filtered_val

def wrap2pi(ang):
    """
    Returns given angle in 
    [-pi, +pi] 
    """
    ang = ang % (2*np.pi)
    if ang > np.pi:
        ang = np.pi - ang
        return -(np.pi + ang)
    else:
        return ang

def decomposed_conv2d(arr,x_kernel,y_kernel):
    """
    Apply two 1D kernels as a part of a 2D convolution.
    The kernels must be the decomposed from a 2D kernel 
    that originally is intended to be convolved with the array.
    Effectively applies convolution in the following manner:
    G = [y_1,y_2,y_3]*[x_1,x_2,x_3]'
    G*A = [y_1,y_2,y_3]*[ [x_1,x_2,x_3]' * A ]
    Inputs:
    - x_kernel: Column vector kernel
    - y_kernel: Row vector kernel
    """
    arr = np.apply_along_axis(lambda x: np.convolve(x, x_kernel, mode='same'), 0, arr)
    arr = np.apply_along_axis(lambda x: np.convolve(x, y_kernel, mode='same'), 1, arr)
    return arr

def start():
    """
    Initialize class object and define subs. pubs. 
    etc. as class attributes and 
    define main loop if needed
    """
    # For debug add arg to init_node: log_level=rospy.DEBUG
    rospy.init_node("pba", log_level=rospy.WARN)
    # rospy.on_shutdown(shutdown_hook)
    nh = NodeHandle()
    nh.injection_time = rospy.Time()

    while not rospy.is_shutdown(): 
        nh.get_params()
        nh.navigate()
        nh.inject(nh.diminishing_rate) # we don't want exit_cue_rand or avoid_nicely to block this (need to publish 0 immediately)
        nh.iters += 1
        nh.rate.sleep() # loop is held out of rate by turn_theta, sleep_w_s and avoid_nicely (by extension, exit_cue_rand)


# start from command-line
if __name__ == '__main__':
    start()
