#!/usr/bin/env python3
# Standard library imports
from datetime import datetime
import time
import random
import json # for publishing dictionary as encoded string
from copy import deepcopy
# External library imports
import numpy as np
# ROS imports
import rospy
from geometry_msgs.msg import Twist, Pose2D
from std_msgs.msg import UInt8, Bool
from nav_msgs.msg import Odometry
from kobot_msgs.msg import range_n_bearing_sensor, floor_sensor
# ROS-TF2 imports
import tf_conversions as tfc

class BeeclustDriver(object):
    def __init__(self):
        ## Get Kobot namespace information
        self.kobot_namespace=rospy.get_namespace().strip("/")

        odom_freq=20
        if rospy.has_param('odom_freq'):
            odom_freq=rospy.get_param('odom_freq')
        self.odom_rate=rospy.Rate(odom_freq)

        ## Get parameters from ROS parameter server
        self.get_params()

        ## Attribute initialization
        # Timing information
        self.nav_start_time = rospy.Time.now()
        self.cue_lock_time = rospy.Time.now() # prevent cue navigation for some time if it fails
        self.iters = 0

        # Logical flags
        self.obs_detected = False
        self.robot_detected = False
        self.going_cue = False
        self.navigate_disabled = False # previously called sub_lock - disables self.navigate()

        # Sensor attributes
        self.robot_pose = Pose2D()
        self.I_c = 0
        self.I_avg_prev = 0
        self.range_prev = [0]*self.rb_sensors #12 sensors, works with any multiple of 4
        self.is_robot_prev = [0]*self.rb_sensors

        self.robot_pose = Pose2D()
        self.I_c = 0
        self.I_avg_prev = 0

        # message initalizers w/ def. vals.
        self.obj_msg = Bool()
        self.obj_msg = False
        self.intensity_msg = UInt8()
        self.intensity_msg = 0
        # Message initialization
        self.intensity_msg = UInt8()
        self.intensity_msg = 0
        self.landmark_dict = {}

        # first define publishers to not get any err.
        self.nav_vel_pub = rospy.Publisher(
            "nav_vel",
            Twist, queue_size=1)

        # publisher for switching between vel. and pos. control
        self.move_lock_pub = rospy.Publisher(
            "move_lock",
            Bool, queue_size=1)

        rospy.Subscriber("sensors/range_n_bearing",
                         range_n_bearing_sensor,
                         self.rb_callback,
                         queue_size=1)

        rospy.Subscriber("sensors/floor",
                         floor_sensor,
                         self.intensity_callback,
                         queue_size=1)

        # actual Kobots use wheel_odom as a separate entity than odom
        # should be publishing only one among these odom topics
        if self.odom_topic == "odom":
            rospy.Subscriber('odom',
                            Odometry,
                            self.odom_callback, queue_size=1)
        elif self.odom_topic == "wheel_odom":
            rospy.Subscriber('wheel_odom',
                            Odometry,
                            self.odom_callback, queue_size=1)
        else:
            rospy.logwarn("Invalid pose_select parameter. Defaulting to odom.")
            rospy.Subscriber('odom',
                            Odometry,
                            self.odom_callback, queue_size=1)

    ## Methods
    # Parameter access
    def get_params(self):
        """
        BEECLUST parameters are checked for every loop and
        updated if necessary
        """
        if rospy.has_param('debug_mode'):
            self.navigator_debug_mode = rospy.get_param('debug_mode')
        else:
            self.navigator_debug_mode = False
            
        if rospy.has_param('beeclust_params'):
            # fetch a group (dictionary) of parameters
            params = rospy.get_param('beeclust_params')
            if self.navigator_debug_mode:
                self.w_max = 20
            else:    
                self.w_max = params['w_max']
            self.u_max = params['u_max']
            self.min_angular_err = params['min_ang_err']
            self.K_p = params['K_p']
            self.zeta_range = params['zeta_range']
            self.zeta_is_robot = params['zeta_is_robot']
            self.obs_detection_thresh_1 = params['obs_detection_thresh_1']
            self.obs_detection_thresh_2 = params['obs_detection_thresh_2']
            self.obs_detection_thresh_3 = params['obs_detection_thresh_3']
            self.cue_exit_robot_thresh_1 = params['cue_exit_robot_thresh_1']
            self.cue_exit_robot_thresh_2 = params['cue_exit_robot_thresh_2']
            self.obs_robot_detection_thresh_1 = params[
                'obs_robot_detection_thresh_1']
            self.obs_robot_detection_thresh_2 = params[
                'obs_robot_detection_thresh_2']
            self.obs_robot_detection_thresh_3 = params[
                'obs_robot_detection_thresh_3']
            self.robot_detection_thresh = params['robot_detection_thresh']
            self.avoid_thresh = params['avoid_thresh']
            self.avoid_failure_thresh = params['avoid_failure_thresh']
            self.I_thresh = params["I_thresh"]
            self.I_const = params["I_const"]
            self.max_rand_ang = params["max_rand_ang"]
            self.odom_topic = params["odom_topic"] # either "odom" or "wheel_odom"
            self.rb_sensors = params["rb_sensors"] # works with any multiple of 4
            self.turn_theta_max_iters = params["turn_theta_max_iters"] # divide by odom_rate to find duration in seconds
            self.dynamic_beeclust_params = params['dynamic_beeclust_params']
        else:  # feed default vals
            rospy.logwarn_once("[{}/beeclust.py]: Parameters are assigned default values!".format(self.kobot_namespace))
            self.w_max = 90
            self.u_max = 0.12
            self.min_angular_err = 0.1
            self.K_p = 1.0
            self.zeta_range = 1.0
            self.zeta_is_robot = 1.0
            self.obs_detection_thresh_1 = 165
            self.obs_detection_thresh_2 = 155
            self.obs_detection_thresh_3 = 155
            self.obs_robot_detection_thresh_1 = 165
            self.obs_robot_detection_thresh_2 = 155
            self.obs_robot_detection_thresh_3 = 155
            self.cue_exit_robot_thresh_1 = 165
            self.cue_exit_robot_thresh_2 = 145
            self.robot_detection_thresh = 165
            self.avoid_thresh = 165
            self.avoid_failure_thresh = 165
            self.I_thresh = 150
            self.I_const = 1000
            self.max_rand_ang = 0.0
            self.odom_topic = "odom" # either "odom" or "wheel_odom"
            self.rb_sensors = 12 # works with any multiple of 4
            self.turn_theta_max_iters = 420 # divide by odom_rate to find duration in seconds
            self.dynamic_beeclust_params = True # fetch parameters later if they are not currently available
        
        if (self.rb_sensors % 4) != 0:
            # range and bearing subsystem must have a multiple of 4 sensor inputs.
            # if this value cannot be asserted, use the default value indicated above
            self.rb_sensors = 12

        self.frontal_rb_list = [i for i in range(self.rb_sensors) if 3*self.rb_sensors/4<i<self.rb_sensors or 0<i<self.rb_sensors/4]
        self.side_rb_list = [int(self.rb_sensors/4), int(3*self.rb_sensors/4)]

    # Callback methods
    def rb_callback(self, data):
        """
        Range values from the range and bearing
        """
        # preallocate flags before processing sensor data
        self.obs_detected = False
        self.robot_detected = False
        self.rb = range_n_bearing_sensor()
        self.rb = data.range_n_bearing

        range_prev = deepcopy(self.range_prev)
        is_robot_prev = deepcopy(self.is_robot_prev)
        self.range_prev = [0]*self.rb_sensors
        self.is_robot_prev = [0]*self.rb_sensors

        self.detected_sensor_angs = []

        for indx, sensor_reading in enumerate(data.range_n_bearing):

            # filter range values and update prev filtered lists
            range_val = moving_average_filter(sensor_reading.range,
                                              range_prev[indx], self.zeta_range)
            self.range_prev[indx] = range_val
            # filter is_robot vals.
            is_robot = moving_average_filter(sensor_reading.is_robot,
                                             is_robot_prev[indx], self.zeta_is_robot)
            self.is_robot_prev[indx] = is_robot
            # range_val = int(range_val)
            if is_robot < 0.5:
                is_robot = False
            else:
                is_robot = True

            if not is_robot:
                if range_val < self.obs_detection_thresh_1:
                    if indx in [0]:
                        # 0 is the front facing sensor of the robot
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_1, indx)
                    elif indx in self.frontal_rb_list:
                        # 1, 7  front left and right facing sensors for 8 sensor setup
                        # 1, 2, 10, 11 front left and right facing sensors for 12 sensor setup
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_2, indx)

                    elif indx in self.side_rb_list:
                        # 2, 6 left and right facing sensors for 8 sensor setup
                        # 3, 9 left and right facing sensors for 12 sensor setup
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_3, indx)

            else:
                if range_val < self.robot_detection_thresh:
                    # all sensors used for robot detection
                    self.robot_detected = True
                    # robots in extremely close proximity are treated as obstacles
                    if indx in [0]:
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_1, indx)
                    elif indx in self.frontal_rb_list:
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_2, indx)
                    elif indx in self.side_rb_list:
                        self.obs_detection(range_val, self.obs_robot_detection_thresh_3, indx)
            if self.detected_sensor_angs != []:
                self.detected_sensor_ang = sum(self.detected_sensor_angs) / float(len(self.detected_sensor_angs))

    def intensity_callback(self, data):
        """
        Intensity vals. from the floor sensors
        """
        I_list = data.intensity
        # I_list = [ord(intensity) for intensity in data.intensity] # use this if floor_sensor() msg has non-standard int typing

        I_sum = 0
        for I in I_list:
            I_sum += I
        I_avg = I_sum / len(I_list)
        self.I_c = I_avg

    def odom_callback(self, data):
        """
        Odom callback for 2-D robot pose
        """
        quaternion = data.pose.pose.orientation
        explicit_quat = [quaternion.x, quaternion.y, quaternion.z, quaternion.w]
        _, _, yaw = tfc.transformations.euler_from_quaternion(explicit_quat)
        x, y = data.pose.pose.position.x, data.pose.pose.position.y
        theta = yaw
        self.robot_pose = Pose2D(x=x, y=y, theta=theta)

    # Helper methods
    def obs_detection(self, range_val, range_thresh, indx):
        """
        Decide if an obstacle is detected based on the inputs
        """
        if range_val < range_thresh:
            self.obs_detected = True
            self.detected_sensor_angs.append(wrap2pi(indx * 2*np.pi/self.rb_sensors))

    def avoid_nicely(self):
        """
        How the robot avoids objects randomly in BEECLUST and its variants.
        The random action must be within a space where the robot can successfully avoid its obstacles.
        If such a random action does not exist, the robot should turn in a random direction until it is freed from obstacles.
        """
        theta = random.uniform(+np.pi/2 + self.max_rand_ang,
            3*np.pi/2 - self.max_rand_ang)
        theta += self.detected_sensor_ang
        theta = wrap2pi(theta)
        # find corresponding rb after rotation
        decision_indx = int(theta / (2*np.pi/self.rb_sensors))
        if self.range_prev[decision_indx] > self.avoid_thresh and self.range_prev[(decision_indx+1)%self.rb_sensors] > self.avoid_thresh:
            # if random decision gets us out follow it
            self.turn_theta(theta)
            self.publish_twist(0.1, 0)
        else:
            # random walk can't get robot into the clear, turn until front is cleared
            rate = rospy.Rate(20)
            is_ccw = random.choice([True, False])
            while True:
                if self.range_prev[0] > self.avoid_failure_thresh:
                    self.publish_twist(0.1, 0)
                    break
                else:
                    if is_ccw:
                        self.publish_twist(0, 1.2) 
                    else:
                        self.publish_twist(0, -1.2)
                rate.sleep()

    def sleep_w_s(self, I_c):
        """
        Compute wait time from the cue intensity reading, then wait by that amount
        """
        # stop and rotate
        self.publish_twist(0, 0)
        float_I = float(I_c)
        # compute w_s
        w_s = self.w_max*(float_I**2/(float_I**2 + self.I_const))
        # mark the start time
        start_time = rospy.Time.now()
        rate = rospy.Rate(1)
        iters = 0
        while not rospy.is_shutdown():
            # calculate time passed in secs
            time_passed = rospy.Time.now() - start_time
            time_passed = time_passed.secs
            if iters % 75 == 0:
                rospy.loginfo("[{}]: waiting on cue for {:.2f} sec".format(
                    self.kobot_namespace,w_s-time_passed))
            if w_s - time_passed < 0:
                # time is up
                break
            rate.sleep()
            #add a tick to the timer
            iters += 1

    def exit_cue_rand(self):
        """
        Tries to exit from cue by rotating randomly
        and if front sensors doesn't detect any robot
        tries to get away from the cue for 5s unless
        any other robot or obstacle seen in front
        """
        # apply random turn action
        theta = random.uniform(-np.pi, np.pi)
        self.turn_theta(theta)
        # go straight for 6 seconds to leave cue area
        rate = rospy.Rate(20)
        # each loop takes 0.05 s
        for _ in range(120):
            if rospy.is_shutdown():
                return
            self.publish_twist(0.1, 0)
            # check whether exit path from cue is occluded by another robot
            if self.rb[0].is_robot and self.rb[0].range < self.cue_exit_robot_thresh_1:
                self.publish_twist(0, 0)
                return
            for indx in self.frontal_rb_list:
                if self.rb[indx].is_robot and self.rb[indx].range < self.cue_exit_robot_thresh_2:
                    self.publish_twist(0, 0)
                    return
            if self.obs_detected:
                self.publish_twist(0, 0)
                return
            rate.sleep()

    def turn_theta(self, theta_rel):
        """
        Turns by theta relative to the current 
        orientation in a closed loop manner 
        from the feedback of the odometry callback
        """
        self.publish_move_lock(True)
        self.navigate_disabled = True
        # desired angle is current angle + rel angle
        theta_des = self.robot_pose.theta + theta_rel
        # initialize timer ticks for screen outputs and break conditions
        iters = 0
        self.publish_twist(0,0) # ensure that the robot is stationary before turning action
        # angle control loop should be blocking
        while not rospy.is_shutdown():
            # error is the difference among current angle and desired angle, limited to -pi, +pi
            angle_err = wrap2pi(theta_des - self.robot_pose.theta)
            if iters % 20 == 0 and self.going_cue:
                rospy.logdebug("[{}]: Current angular error: {}".format(self.kobot_namespace,angle_err*np.pi/180))

            if abs(angle_err) <= self.min_angular_err:
                if self.going_cue:
                    rospy.loginfo("Turn action completed! Switching back to the main loop.")
                self.publish_twist(0,0) # ensure that the robot applies the subsequent twist command by stopping first
                # reached the target angle
                break
            omega = angle_err * self.K_p
            # control action
            self.publish_twist(0, omega)
            self.odom_rate.sleep()
            iters += 1

            if iters == self.turn_theta_max_iters:
                rospy.logwarn("[{}/beeclust.py]: turn_theta cannot settle. Breaking.".format(self.kobot_namespace))
                break
        # stop
        self.navigate_disabled = False

    def publish_move_lock(self, bool_val):
        """
        Send state of the lock to the pose_controller
        when state is true velocity controller takes on the command
        when state is false position controller takes on the command
        """
        move_lock_msg = Bool()
        move_lock_msg = bool_val
        self.move_lock_pub.publish(move_lock_msg)

    def publish_twist(self, x_vel, theta_vel):
        """
        Publish ref. vals. for vel. controller
        """
        twist_msg = Twist()
        twist_msg.linear.x = x_vel
        twist_msg.angular.z = theta_vel

        self.nav_vel_pub.publish(twist_msg)

    # Primary method definitions
    def navigate(self):
        """
        Main navigator function to use with aggregation algorithms.
        Called once every loop. Executes the finite state machine for the robot.\n
        LBA behavior:
        Random walk unless in cue area, or issued a turn/move command which locks the navigator.
        """
        # TODO: going_cue flags should be moved to top level if possible to lock out the LBA navigation while predictive navigation is online.
        if self.navigate_disabled:
            if self.iters % 80 == 0:
                rospy.loginfo("[{}]: Navigation is disabled!".format(self.kobot_namespace))
            # ignore any new messages, still processing the latest
            return

        if self.obs_detected:
            if self.I_c < self.I_thresh:
                self.publish_twist(0, 0)
                # avoidance behaviour
                self.avoid_nicely()
            else:
                self.robot_detected = True

        if self.robot_detected or self.navigator_debug_mode:
            if self.I_c > self.I_thresh:
                rospy.loginfo("[{}]: move_lock engaged and going_cue disabled - encountered a robot in cue area.".format(self.kobot_namespace))
                self.publish_move_lock(True)
                self.going_cue = False
                self.publish_twist(0, 0)
                # we are in the cue area
                self.sleep_w_s(self.I_c)
                # slept enough now try to exit from cue
                self.exit_cue_rand()

        if not self.obs_detected and not self.robot_detected: #and not self.going_cue:
            # roaming phase - no detection from the frontal range-and-bearing sensors
            self.publish_twist(self.u_max, 0)

def moving_average_filter(val, filtered_val_prev, zeta):
    """
    Basic moving average filter
    zeta = 1 -> ignore prev. vals
    zeta = 0 -> ignore current val
    """
    filtered_val = (1-zeta)*filtered_val_prev + zeta*val
    return filtered_val

def wrap2pi(ang):
    """
    Returns given angle in 
    [-pi, +pi] 
    """
    ang = ang % (2*np.pi)
    if ang > np.pi:
        ang = np.pi - ang
        return -(np.pi + ang)
    else:
        return ang

def start():
    """
    Initialize class object and define subs. pubs. 
    etc. as class attributes and 
    define main loop if needed
    """
    # For debug add arg to init_mode log_level=rospy.DEBUG
    rospy.init_node("beeclust")
    # rospy.on_shutdown(shutdown_hook)
    driver = BeeclustDriver()
    freq = 20
    if rospy.has_param('beeclust_freq'):
        freq = rospy.get_param('beeclust_freq')
    rate = rospy.Rate(freq)  # Hz
    while not rospy.is_shutdown():
        driver.get_params()
        driver.navigate()
        rate.sleep()


# start from command-line
if __name__ == '__main__':
    start()
