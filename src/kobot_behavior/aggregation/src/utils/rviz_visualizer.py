#!/usr/bin/env python3
# Primary imports
import numpy as np
# ROS imports
import rospy
# ROS standard messages
## TODO: add Vicon position datatype for real robot experiments
from geometry_msgs.msg import Point, Pose, Quaternion
from nav_msgs.msg import OccupancyGrid, MapMetaData
from kobot_msgs.msg import pheromone_grid
from visualization_msgs.msg import Marker, MarkerArray

class NodeHandle:
  """
  RViz visualization kit for the Kobot simulator and virtual pheromone driver.\n
  """
  def __init__(self):
    self.show_robots = True # unless declared False by a pheromone driver specification
    # Access parameter server to get count of robots and arena information
    self.get_params()

    # Pheromone data communication
    if self.show_pheromone:
      # Initialize grid parameters
      self.init_pheromone_metadata()

      # Pheromone callback data
      self.pheromone_grid = np.array([])
      # Initialize subscribers
      self.pheromone_data_sub = rospy.Subscriber(name="/pheromone/data",
                                                    data_class= pheromone_grid,
                                                    callback= self.pheromone_callback,
                                                    queue_size= 10
                                                    )
      ## RViz publishers
      self.pheromone_grid_pub = rospy.Publisher(name="/pheromone/grid",
                                                    data_class= OccupancyGrid,
                                                    queue_size= 10)

    self.rviz_markers_pub = rospy.Publisher(name= "visualization_marker_array",
                        data_class= MarkerArray,
                        queue_size=1)

    # Construct a view of the arena in RViz (done once, as parent frame is static)
    # If robots are spawned, these will be repetitively published along with the robots
    self.marker_array = MarkerArray()
    if self.show_arena:
      self.generate_rviz_markers()

  def get_params(self):
    """
    Get parameters for simulation or robot trials
    by default checks sim_params_aggregation.yaml and pheromone.yml or any identical parameter set

    You should introduce these parameters privately,
    or through a rosbash API call where the ns is set as "/pheromone".

    /pheromone/sim, /pheromone/swarm, /pheromone/node /pheromone/floor/outer_radius_of_floor refer to sim_params_aggregation.yaml

    /pheromone/frequency, /pheromone/publisher, /pheromone/subscriber,
    /pheromone/robot_count, /pheromone/pose_topic,
    /pheromone/robot_radius, /pheromone/arena server access namespaces refer to pheromone.yml
    """

    # Private node parameters
    ## Operation frequency, node/param namespaces
    self.frequency = rospy.get_param('~frequency', default= 120)
    pheromone_ns = rospy.get_param('~pheromone_ns', default= '/pheromone')
    sim_ns = rospy.get_param('~sim_ns', default= '/sim/sim_2d_node')
    self.robot_ns = rospy.get_param('~robot_ns', default= "kobot")
    ## Pheromone related visualization parameters
    self.visual_threshold = rospy.get_param('~visual_threshold', default= 0.5) # RViz - cutoff intensity
    self.visual_saturation = rospy.get_param('~visual_saturation', default= 200) # RViz - scale down visualizer depending on pheromone readout range
    ## Enable or disable RViz visuals
    self.show_pheromone = rospy.get_param('~show_pheromone', default= True)
    self.show_robots = rospy.get_param('~show_robots', default= True)
    self.show_arena = rospy.get_param('~show_arena', default= True)
    
    if self.show_pheromone:
      # unique to pheromone.yml
      self.grid_resolution = rospy.get_param(f'{pheromone_ns}/resolution', default= 0.02) # defaults to a third of robot radius in meters

    if rospy.has_param(f'{sim_ns}/sim'):
      # expecting inputs scaled to millimeters, outputs in meters
      width_pixels = rospy.get_param(f'{sim_ns}/sim/width', default=420)
      height_pixels = rospy.get_param(f'{sim_ns}/sim/height', default=420)
      pixel_p_mm = rospy.get_param(f'{sim_ns}/sim/pixel_p_mm', default=0.15)
      self.tf_parent_frame = rospy.get_param(f'{sim_ns}/sim/tf_parent_frame', default='vicon/world')

      self.arena_width = width_pixels / pixel_p_mm / 1000
      self.arena_height = height_pixels / pixel_p_mm / 1000
    elif rospy.has_param(f'{pheromone_ns}/arena'):
      # expecting inputs in meters
      self.arena_width = rospy.get_param(f'{pheromone_ns}/arena/width', default=2.8)
      self.arena_height = rospy.get_param(f'{pheromone_ns}/arena/height', default=2.8)
      self.tf_parent_frame = rospy.get_param(f'{pheromone_ns}/arena/tf_parent_frame', default='vicon/world')

    if rospy.has_param(f'{sim_ns}/floor/outer_radius_of_floor'):
      # expecting inputs scaled to millimeters, outputs in meters
      self.outer_cue_radius = rospy.get_param(f'{sim_ns}/floor/outer_radius_of_floor', default=350) / 1000
    elif rospy.has_param(f'{pheromone_ns}/arena'):
      self.outer_cue_radius = rospy.get_param(f'{pheromone_ns}/arena/outer_cue_radius', default=350) / 1000

    if rospy.has_param(f'{sim_ns}/floor/inner_radius_of_floor'):
      # expecting inputs scaled to millimeters, outputs in meters
      self.inner_cue_radius = rospy.get_param(f'{sim_ns}/floor/inner_radius_of_floor', default=350) / 1000
    elif rospy.has_param(f'{pheromone_ns}/arena'):
      self.inner_cue_radius = rospy.get_param(f'{pheromone_ns}/arena/inner_cue_radius', default=350) / 1000

    if rospy.has_param(f'{sim_ns}/swarm'):
      self.robot_count = rospy.get_param(f'{sim_ns}/swarm/num_of_drone', default=4)
    elif rospy.has_param(f'{sim_ns}/robot_count'):
      self.robot_count = rospy.get_param(f'{sim_ns}/robot_count', default=4)
    else:
      rospy.logwarn("[rviz_visualizer.py]: Robot count unspecified. Defaulting to 4.")
      self.robot_count = 4

    if rospy.has_param(f'{sim_ns}/node'):
      self.pose_name = rospy.get_param(f'{sim_ns}/node/pose_topic')
    elif rospy.has_param(f'{pheromone_ns}/pose_topic'):
      self.pose_name = rospy.get_param(f'{pheromone_ns}/pose_topic')
    else:
      # not expecting the topic name to change too frequently
      self.pose_name = 'pose2d_global'

    # robot radii can be also used to specify grid resolution if desired
    if rospy.has_param(f'{sim_ns}/drone'):
    # expecting inputs in millimeters, converting to meters
      self.robot_radius = rospy.get_param(f'{sim_ns}/drone/radius', default=60) / 1000
    elif rospy.has_param(f'{pheromone_ns}/robot_radius'):
      self.robot_radius = rospy.get_param(f'{pheromone_ns}/robot_radius', default=60) / 1000
    else:
      self.robot_radius = 60 / 1000

  # Callbacks and publisher topic formatter functions
  def pheromone_callback(self,msg):
    self.pheromone_grid = np.asarray(msg.grid).reshape((msg.height,msg.width))
  def grid_topic(self,grid):
    self.pheromone_grid_pub.publish(grid)

  def init_pheromone_metadata(self):
    """
    Initialize RViz MapMetaData using pheromone grid parameters.\n
    It should be noted that the grid itself is not initialized here, and received through a callback.\n
    The metadata generated is required to fit the OccupancyGrid geometry in RViz.\n
    """
    origin_point = Point(x= -self.arena_width/2, y= -self.arena_height/2 ,z= 0)
    grid_origin = Pose(position= origin_point, orientation= Quaternion(0,0,0,1))
    width_cells = int(np.ceil(self.arena_width/self.grid_resolution)) + 1 # to prevent undercutting add 1
    height_cells = int(np.ceil(self.arena_height/self.grid_resolution)) + 1 # to prevent undercutting add 1
    self.rviz_metadata = MapMetaData(map_load_time= rospy.Time.now(),
                                         resolution= self.grid_resolution,
                                         width= width_cells,
                                         height= height_cells,
                                         origin= grid_origin)

  def generate_rviz_markers(self):
    """
    Generate rviz markers to publish. Uses visualizations_msgs/MarkerArray.\n
    The markers are to be fixed to an inertial frame (most likely the localization frame, such as Vicon).\n
    Read the following for more details:\n
    http://wiki.ros.org/rviz/DisplayTypes/Marker
    http://docs.ros.org/en/api/visualization_msgs/html/msg/Marker.html
    https://docs.ros.org/en/fuerte/api/rviz/html/marker__array__test_8py_source.html
    """
    outer_cue_marker = Marker()
    outer_cue_marker.id = 0
    outer_cue_marker.header.frame_id = self.tf_parent_frame
    outer_cue_marker.type = outer_cue_marker.CYLINDER
    outer_cue_marker.action = outer_cue_marker.ADD
    outer_cue_marker.pose.position.x = 0.0
    outer_cue_marker.pose.position.y = 0.0
    outer_cue_marker.pose.position.z = 0.0
    outer_cue_marker.pose.orientation.w = 1.0
    outer_cue_marker.scale.x = self.outer_cue_radius *2
    outer_cue_marker.scale.y = self.outer_cue_radius *2
    outer_cue_marker.scale.z = 0.2
    outer_cue_marker.color.a = 0.5
    outer_cue_marker.color.r = 0.7
    outer_cue_marker.color.g = 0.7
    outer_cue_marker.color.b = 0.7
    self.marker_array.markers.append(outer_cue_marker)

    inner_cue_marker = Marker()
    inner_cue_marker.id = 1
    inner_cue_marker.header.frame_id = self.tf_parent_frame
    inner_cue_marker.type = inner_cue_marker.CYLINDER
    inner_cue_marker.action = inner_cue_marker.ADD
    inner_cue_marker.pose.position.x = 0.0
    inner_cue_marker.pose.position.y = 0.0
    inner_cue_marker.pose.position.z = 0.0
    inner_cue_marker.pose.orientation.w = 1.0
    inner_cue_marker.scale.x = self.inner_cue_radius *2
    inner_cue_marker.scale.y = self.inner_cue_radius *2
    inner_cue_marker.scale.z = 0.2
    inner_cue_marker.color.a = 0.5
    inner_cue_marker.color.r = 1.0
    inner_cue_marker.color.g = 1.0
    inner_cue_marker.color.b = 1.0
    self.marker_array.markers.append(inner_cue_marker)

    border = Marker()
    border.id = 2
    border.header.frame_id = self.tf_parent_frame
    border.type = border.LINE_STRIP
    border.action = border.ADD
    border.pose.position.x = 0.0
    border.pose.position.y = 0.0
    border.pose.position.z = 0.0
    border.pose.orientation.w = 1.0
    border.scale.x = 0.02
    border.color.a = 1.0
    border.color.r = 1.0
    border.color.g = 0.0
    border.color.b = 0.0
    border.points = [
      Point(x= -self.arena_width/2,y= -self.arena_height/2),
      Point(x= -self.arena_width/2,y= self.arena_height/2),
      Point(x= self.arena_width/2,y= self.arena_height/2),
      Point(x= self.arena_width/2,y= -self.arena_height/2),
      Point(x= -self.arena_width/2,y= -self.arena_height/2),
    ]
    self.marker_array.markers.append(border)
    self.rviz_markers_pub.publish(self.marker_array)

  def generate_rviz_robots(self):
    """
    Generate rviz markers to publish. Uses visualizations_msgs/MarkerArray.\n
    The markers are to be fixed to mobile robots, and must be updated as frequently as possible.\n
    Read the following for more details:\n
    http://wiki.ros.org/rviz/DisplayTypes/Marker
    http://docs.ros.org/en/api/visualization_msgs/html/msg/Marker.html
    https://docs.ros.org/en/fuerte/api/rviz/html/marker__array__test_8py_source.html
    """
    robot_markers = MarkerArray()
    for id in range(self.robot_count):
      marker = Marker()
      marker.id = 3 + id
      marker.header.frame_id = "base_link_{}{}".format(self.robot_ns,id) # or use tf_parent_frame and specify positions below
      marker.type = marker.CYLINDER
      marker.action = marker.ADD
      marker.pose.position.x = 0.0
      marker.pose.position.y = 0.0
      marker.pose.position.z = 0.0
      marker.pose.orientation.w = 1.0
      marker.scale.x = self.robot_radius *2
      marker.scale.y = self.robot_radius *2
      marker.scale.z = 0.2
      marker.color.a = 0.9
      # please keep the robots purple <3
      marker.color.r = 0.5804
      marker.color.g = 0.3412
      marker.color.b = 0.9216
      robot_markers.markers.append(marker)

    # do not forget the arena markers
    try:
      if self.show_arena:
        robot_markers.markers.extend(self.marker_array.markers)
      self.rviz_markers_pub.publish(robot_markers)
    except AttributeError:
      # the initialization isn't done yet
      pass

  # Functional methods
  def visual_grid_prep(self,grid):
    """
    Prepare an OccupancyGrid message to visualize the current status of pheromones. Scale the range of 0 to maximum pheromone value to 0-100.

    Inputs:
    - grid (np.array): Grid of pheromone values over the arena.\n
    Outputs:
    - rviz_grid (OccupancyGrid): Grid data that is visualizable in RViz. Pheromone grid must be in row-major order (make a 1D list of all rows, starting from index 0)
    """
    g = grid.copy()
    # Apply thresholding to empty out sites below a value
    g = np.where(g > self.visual_threshold, g, 0.0)
    # Boost weak pheromones to visible levels
    g = np.where(np.logical_and(self.visual_threshold < g, g <= 1.5), 1.5, g)
    # Visual saturation to keep weak pheromones visible
    g = np.where(g <= self.visual_saturation, g, self.visual_saturation)
    # scale the grid down to 0-100
    # max_val = np.amax(grid)
    # if max_val != 0.0:
    #     scaled_grid = 100*grid/max_val
    # else:
    #     scaled_grid = grid.copy()
    scaled_grid = 100*g/self.visual_saturation

    rviz_grid = OccupancyGrid()
    rviz_grid.header.stamp = rospy.Time.now()
    rviz_grid.header.frame_id = self.tf_parent_frame
    rviz_grid.info = self.rviz_metadata
    rviz_grid.data = np.flipud(scaled_grid).ravel().astype(np.uint8) # need to flip, RViz does array ordering from bottom row up
    return rviz_grid

## Main function
def start():
  """
  Main function for the script.
  See <class: NodeHandle> for more details.
  """
  # Initialize rospy node and rate, along with the script object
  rospy.init_node("rviz_visualizer")
  nh = NodeHandle()
  rate = rospy.Rate(nh.frequency)
  # Main loop
  while not rospy.is_shutdown():
    if nh.show_pheromone and nh.pheromone_grid.shape != (0,):
      ## visualize current pheromone readouts in RViz
      rviz_grid = nh.visual_grid_prep(nh.pheromone_grid)
      nh.grid_topic(rviz_grid)
    if nh.show_robots: # mutually exclusive - if showing robots, place arena markers within
      nh.generate_rviz_robots() # keep updating while running
    elif nh.show_arena: # if not showing robots keep showing the arena instead
      nh.generate_rviz_markers() 
    rate.sleep()

if __name__ == "__main__":
  start()