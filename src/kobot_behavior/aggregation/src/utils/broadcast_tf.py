#!/usr/bin/env python3
#Primary imports
import numpy as np
#ROS imports
import rospy
#ROS standard messages
from nav_msgs.msg import Odometry
from geometry_msgs.msg import TransformStamped, Vector3, Quaternion, Pose2D
#TF2 packages
import tf2_ros as tf2
import tf_conversions as tfc 
# import tf as tfc #temporary until docker issues are fixed

class BroadcastTF(object):
  """
  Use this to bridge generated odom messages from either the simulator 
  or the wheel odometry subsystem and the TF requirements of the behavior scripts. 
  Also set up the static transform publishers. Migrated to tf2_ros from tf.
  """
  def __init__(self):
    #get namespace from arg
    self.kobot_namespace=rospy.get_namespace().strip("/")

    #pull parameters
    self.get_params()

    # actual Kobots use wheel_odom as a separate entity than odom
    # should be publishing only one among these odom topics
    if self.odom_topic == "odom":
        rospy.Subscriber('odom',
                        Odometry,
                        self.odom_cb, queue_size=1)
    elif self.odom_topic == "wheel_odom":
        rospy.Subscriber('wheel_odom',
                        Odometry,
                        self.odom_cb, queue_size=1)
    else:
        rospy.logwarn("Invalid pose_select parameter. Defaulting to odom.")
        rospy.Subscriber('odom',
                        Odometry,
                        self.odom_cb, queue_size=1)
    
    rospy.Subscriber("pose2d_global",
                      Pose2D,
                      self.pose2d_global_cb,
                      queue_size=1)

    rospy.Subscriber("ics",
                     Pose2D,
                     self.ics_cb,
                     queue_size=1)
    
    self.broadcaster = tf2.TransformBroadcaster()
    self.static_broadcaster = tf2.StaticTransformBroadcaster()

    self.begin_broadcast = False
    self.begin_global_broadcast = False
    self.prev_stamp = None
    self.prev_static_stamp = None
    self.global_transform = None
    self.global_enabled = True
    self.ics = None

  def get_params(self):
    """
    Get parameters from the rosparam server.
    """
    if rospy.has_param('broadcast_tf'):
      params = rospy.get_param('broadcast_tf')
      # Read aggregation/config/tf_config.yml for details.
      self.tf_world_frame = params['tf_world_frame']
      self.tf_world_child_frame = params['tf_world_child']
      self.tf_freq = params['tf_freq']
      self.odom_topic = params['odom_topic']
      self.calculate_ics = params['calculate_ics']

    else:
      self.tf_world_frame = 'vicon/world'
      self.tf_world_child_frame = 'map'
      self.tf_freq = 15
      self.odom_topic = 'odom'
      self.calculate_ics = True
    
    if rospy.has_param('tf_static'):
      #Read aggregation/config/tf_config.yml for details.
      self.tf_static = rospy.get_param('tf_static')
    else: #defaults
      rospy.logwarn("[{}/broadcast_tf] Using default values of tf_static!".format(self.kobot_namespace))
      # rospy.logwarn("[TF BROADCASTER]: WORLD IS COINCIDENT TO MAP")
      # # if world is coincident with map
      # self.tf_static = [
      #                   {"frame_id": "vicon/world",
      #                   "child_frame_id": "map",
      #                   "translation": [0,0,0],
      #                   "rotation": [0,0,0,1]
      #                   },
      #                   {"frame_id": "map",
      #                   "child_frame_id": "odom",
      #                   "translation": [0,0,0],
      #                   "rotation": [0,0,0,1]
      #                   },
      #                   {"frame_id": "base_link",
      #                   "child_frame_id": "cam_link",
      #                   "translation": [0,0,0],
      #                   "rotation": [0,0,0,1]
      #                   },
      #                 ]

      # if true global positions are to be known by tf
      self.tf_static = [
                        {"frame_id": "map",
                        "child_frame_id": "odom",
                        "translation": [0.0,0.0,0.0],
                        "rotation": [0.0,0.0,0.0,1.0]
                        },
                        {"frame_id": "base_link",
                        "child_frame_id": "cam_link",
                        "translation": [0.05,0.0,0.08],
                        "rotation": [-0.5,0.5,-0.5,0.5]
                        },
                      ]

    # Simulation only!
    # If global frame is assigned to the static transform list, do not handle the global coordinate transformation 
    for p in self.tf_static:
      if self.tf_world_frame in p.values():
        self.global_enabled = False
        self.begin_global_broadcast = True # pose2d_global_cb will not be triggered, ever

  def odom_cb(self,msg):
    self.odom = msg
    self.begin_broadcast = True

  def pose2d_global_cb(self,msg):
    # use this message to know a robot's position in the global frame (simulation only, for debugging purposes)
    self.pose2d_global = msg
    self.begin_global_broadcast = True
  
  def ics_cb(self,msg):
    self.ics = msg

  def odom_broadcast(self):
    """
    Broadcast the odometry topic to a TF2 broadcast.
    Also check for timestamp repeats to avoid data redundancy.
    """
    t = TransformStamped()
    t.header.stamp = rospy.Time.now()
    t.header.frame_id = self.odom.header.frame_id
    t.child_frame_id = self.odom.child_frame_id
    t.transform.translation.x = self.odom.pose.pose.position.x
    t.transform.translation.y = self.odom.pose.pose.position.y
    t.transform.translation.z = self.odom.pose.pose.position.z
    t.transform.rotation.x = self.odom.pose.pose.orientation.x
    t.transform.rotation.y = self.odom.pose.pose.orientation.y
    t.transform.rotation.z = self.odom.pose.pose.orientation.z
    t.transform.rotation.w = self.odom.pose.pose.orientation.w
    
    if self.prev_stamp is not None:
      # if stamp_check(self.prev_stamp,self.odom.header.stamp,1/self.rate):
      if self.prev_stamp == self.odom.header.stamp:
        # rospy.loginfo("Odom transform stamp repeat prevented")
        # do not repeat a TF broadcast
        pass
      else:
        # rospy.loginfo("Broadcast odom transform at {}".format(self.odom.header.stamp.to_sec()))
        self.broadcaster.sendTransform(t)           
    else:
      #the very first TF broadcast, no repeats possible
      self.broadcaster.sendTransform(t)
    #update header stamp
    self.prev_stamp = self.odom.header.stamp

  def pose2d_global_broadcast(self):
    """
    Broadcast the pose2d topic to a TF2 static broadcast.
    Used by the simulator only, as global positioning is only available in the simulator.
    static_broadcast() handles this transform instead if tf_world_frame is assigned as a frame_id in tf_static.yml
    Expect usage of this broadcast by only debugging functions.
    """
    if self.global_transform is None:
      # Generate global static transform once
      t = TransformStamped()
      t.header.frame_id = self.tf_world_frame
      t.child_frame_id = self.tf_world_child_frame + '_' + self.kobot_namespace

      # Calculate the initial conditions. Use this flag if you are confident that your sensor/simulation data doesn't suffer from lag.
      # Use the else block, and supply a topic /ics (Pose2D) for this Kobot's namespace if you want to make sure that the global broadcast is 
      # properly initialized.
      if self.calculate_ics:
        t.header.stamp = rospy.Time.now()
        # t.transform.translation.x = self.pose2d_global.x - self.odom.pose.pose.position.x
        # t.transform.translation.y = self.pose2d_global.y - self.odom.pose.pose.position.y
        # t.transform.translation.z = 0
        kobot_ori = tfc.transformations.euler_from_quaternion([self.odom.pose.pose.orientation.x,
                                                              self.odom.pose.pose.orientation.y,
                                                              self.odom.pose.pose.orientation.z,
                                                              self.odom.pose.pose.orientation.w]
                                                              )
        trans_ori = [0 - kobot_ori[0], 0 - kobot_ori[1], self.pose2d_global.theta - kobot_ori[2]]
        #NOTE: modified for melodic - can revert to star operator if noetic is used
        # t.transform.rotation = Quaternion(*tfc.transformations.quaternion_from_euler(*trans_ori))
        # q = tfc.transformations.quaternion_from_euler(trans_ori[0], trans_ori[1], trans_ori[2]) # first two are not necessary
        q = tfc.transformations.quaternion_from_euler(trans_ori[0], trans_ori[1], trans_ori[2])
        p = [self.odom.pose.pose.position.x, self.odom.pose.pose.position.y, 0]

        p_rot = quaternion_rotate(q,p)

        t.transform.translation.x = self.pose2d_global.x - p_rot[0]
        t.transform.translation.y = self.pose2d_global.y - p_rot[1]
        t.transform.translation.z = 0    

        t.transform.rotation = Quaternion(q[0],q[1],q[2],q[3])
        self.global_transform = t
        self.static_broadcaster.sendTransform(self.global_transform)
        # rospy.logwarn("[broadcast_tf]: Global transform initialized!")
      else:
        if self.ics != None:
          t.header.stamp = rospy.Time.now()
          t.transform.translation.x = self.ics.x
          t.transform.translation.y = self.ics.y
          t.transform.translation.z = 0
          kobot_ori = tfc.transformations.quaternion_from_euler(0,0,self.ics.theta)
          t.transform.rotation = Quaternion(q[0],q[1],q[2],q[3])
          self.global_transform = t
          self.static_broadcaster.sendTransform(self.global_transform)
          # rospy.logwarn("[broadcast_tf]: Global transform initialized!")

    else:
      #update timestamp
      self.global_transform.header.stamp = rospy.Time.now()
      self.static_broadcaster.sendTransform(self.global_transform)

  def static_broadcast(self):
    """
    Add static broadcast functions here to pass to the main loop.
    Assign broadcast parameters through a list of dictionaries in tf_static.yml.
    """
    if self.prev_static_stamp is not None:
      #if stamp_check(self.prev_static_stamp,self.odom.header.stamp,1/self.rate): #making this tied to the odometry transform rate
      if self.prev_static_stamp == self.odom.header.stamp:
        # rospy.loginfo("Static transform stamp repeat prevented")
        #do not repeat a TF broadcast
        pass
      else:
        # rospy.loginfo("Broadcasting static transform")
        for p in self.tf_static:
          #Prepare broadcast message
          data = TransformStamped()
          data.header.stamp = rospy.Time.now()
          if p["frame_id"] == self.tf_world_frame:
            #no namespace variations for parent frame
            data.header.frame_id = p["frame_id"]
          else:
            data.header.frame_id = p["frame_id"] + "_" + self.kobot_namespace
          data.child_frame_id = p["child_frame_id"] + "_" + self.kobot_namespace
          #NOTE: modified for melodic - can revert to star operator if noetic is used
          # data.transform.translation = Vector3(*p["translation"]) #must be an iterable (list or tuple)
          # data.transform.rotation = Quaternion(*p["rotation"]) #must be an iterable (list or tuple)
          data.transform.translation = Vector3(p["translation"][0],p["translation"][1],p["translation"][2])
          data.transform.rotation = Quaternion(p["rotation"][0],p["rotation"][1],p["rotation"][2],p["rotation"][3])
          self.static_broadcaster.sendTransform(data)
        #update previous static header stamp
        # rospy.loginfo("Broadcast static transform at {}".format(self.odom.header.stamp.to_sec()))
        
    else:
      #the very first TF broadcast, no repeats possible
      for p in self.tf_static:
        #Prepare broadcast message
        data = TransformStamped()
        data.header.stamp = rospy.Time.now()
        if p["frame_id"] == self.tf_world_frame:
          #no namespace variations for parent frame
          data.header.frame_id = p["frame_id"]
        else:
          data.header.frame_id = p["frame_id"] + "_" + self.kobot_namespace
        data.child_frame_id = p["child_frame_id"] + "_" + self.kobot_namespace
        #NOTE: modified for melodic - can revert to star operator if noetic is used
        # data.transform.translation = Vector3(*p["translation"]) #must be an iterable (list or tuple)
        # data.transform.rotation = Quaternion(*p["rotation"]) #must be an iterable (list or tuple)
        data.transform.translation = Vector3(p["translation"][0],p["translation"][1],p["translation"][2])
        data.transform.rotation = Quaternion(p["rotation"][0],p["rotation"][1],p["rotation"][2],p["rotation"][3])
        self.static_broadcaster.sendTransform(data)

    #update previous static header stamp
    self.prev_static_stamp = self.odom.header.stamp

# Quaternion utilities for quick testing or TF alternative code
def quaternion_product(q1,q2):
    if type(q1) != Quaternion:
        q1 = Quaternion(*q1)
    if type(q2) != Quaternion:
        q2 = Quaternion(*q2)
    w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z
    x = q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y
    y = q1.w * q2.y + q1.y * q2.w + q1.z * q2.x - q1.x * q2.z
    z = q1.w * q2.z + q1.z * q2.w + q1.x * q2.y - q1.y * q2.x
    return Quaternion(x, y, z, w)

def quaternion_rotate(quaternion, vector):
    q_vector = Quaternion(*vector,0)
    if type(quaternion) != Quaternion:
        quaternion = Quaternion(*quaternion)
    q_result = quaternion_product(quaternion_product(quaternion, q_vector),quaternion_conjugate(quaternion))
    return np.array([q_result.x, q_result.y, q_result.z])

def quaternion_conjugate(q):
    return Quaternion(-q.x, -q.y, -q.z, q.w)

def start():
  """
  Main loop definition
  """
  rospy.init_node("broadcast_tf")
  b = BroadcastTF()
  rate = rospy.Rate(b.tf_freq)
  while not rospy.is_shutdown():
    if b.begin_broadcast and b.begin_global_broadcast:
      # for simulations this corresponds to an odom message being published
      # which signifies that time has been initialized
      # for real robots, the tf relationships will most likely be formed 
      # after the first odom message as well 
      b.odom_broadcast()
      if b.global_enabled:
        # rospy.loginfo_once("[broadcast_tf]: Also broadcasting global positions!")
        b.pose2d_global_broadcast()
      b.static_broadcast()
    rate.sleep() #rate-limit the TF broadcasts for better performance

if __name__ == '__main__':
  start()