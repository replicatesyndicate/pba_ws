#!/usr/bin/env python3
from copy import deepcopy
import rospy
from geometry_msgs.msg import Twist, PoseStamped
from std_msgs.msg import Bool
from nav_msgs.msg import Odometry
from math import atan2, sqrt, pi
import tf_conversions as tfc 
# import tf as tfc #temporary fix against docker issues  


class poseController:

    def __init__(self):
        self.kobot_namespace=rospy.get_namespace().strip("/")
        # unique node (using anonymous=True).
        rospy.init_node('pose_controller', anonymous=True)
        
        #initialize parameters
        self.get_params()

        self.velocity_publisher = rospy.Publisher('nav_vel',
                                                  Twist, queue_size=1)

        self.goal_reached_publisher = rospy.Publisher('goal_reached',
                                                      Bool, queue_size=1)
        self.err_th_prev = 0
        self.pos_err_prev = 0
        self.err_th_sum = 0
        self.err_th_diff = 0
        self.odom = Odometry()
        self.theta = 0
        self.y = 0
        self.x = 0

        self.goal_x=0
        self.goal_y=0
        self.goal_theta=0
        self.move_lock=True
        odom_freq = 30
        if rospy.has_param('~odom_freq'):
            odom_freq=rospy.get_param('~odom_freq')
        self.odom_rate=rospy.Rate(odom_freq)
        self.odom_freq=odom_freq
        # actual Kobots use wheel_odom as a separate entity than odom
        # should be publishing only one among these odom topics
        if self.pose_select == "odom":
            rospy.Subscriber('odom',
                            Odometry,
                            self.odom_callback, queue_size=1)
        elif self.pose_select == "wheel_odom":
            rospy.Subscriber('wheel_odom',
                            Odometry,
                            self.odom_callback, queue_size=1)
        else:
            rospy.logwarn("Invalid pose_select parameter. Defaulting to odom.")
            rospy.Subscriber('odom',
                            Odometry,
                            self.odom_callback, queue_size=1)
        rospy.Subscriber('move_base_simple/goal',
                         PoseStamped,
                         self.pose_goal_callback, queue_size=1)
        rospy.Subscriber('move_lock',
                         Bool,
                         self.move_lock_callback, queue_size=1)
        
        # timing variables
        self.iters = 0

    def get_params(self):
        """
        Params. are checked constantly and is
        updated if necessary
        """
        if rospy.has_param('~pose_controller_params'):
            # fetch a group (dictionary) of parameters
            params = rospy.get_param('~pose_controller_params')
            self.pose_select = params['pose_select'] # odom for simulations, wheel_odom for some actual robot experiments
            self.K_p_lin = params['K_p_lin']
            self.K_p_ang = params['K_p_ang']
            self.K_i_ang = params['K_i_ang']
            self.K_d_ang = params['K_d_ang']
            self.distance_tolerance = params['dist_tol']
            self.angular_tolerance = params['ang_tol']
            self.u_max = params['u_max']
            self.w_max = params['w_max']
            self.pose_control_freq = params['pose_control_freq']
            self.report_interval = params['report_interval']
            self.penalty_max = params['penalty_max']
            self.angular_control_enabled = params['angular_control_enabled']

        else:  # feed default vals
            # rospy.loginfo("Loading default pose controller parameters.")
            self.pose_select = 'odom' # odom for simulations, wheel_odom for some actual robot experiments
            self.distance_tolerance = 0.4
            self.angular_tolerance = 0.15
            self.u_max = 0.20
            self.w_max = 0.8
            self.K_p_lin = 1.5
            self.K_p_ang = 1.0
            self.K_i_ang = 0.0
            self.K_d_ang = 0.0
            self.pose_control_freq = 30
            self.report_interval = 10
            self.penalty_max = 20
            self.angular_control_enabled = False
            

    def odom_callback(self, data):
        """
        For position feedback
        """
        self.x = data.pose.pose.position.x
        self.y = data.pose.pose.position.y
        quaternion = data.pose.pose.orientation
        explicit_quat = [quaternion.x, quaternion.y, quaternion.z, quaternion.w]
        _, _, yaw = tfc.transformations.euler_from_quaternion(explicit_quat)
        self.theta = yaw

    def move_lock_callback(self, data):
        """
        Move lock is used for swithcing b/w pos. and vel.
        controllers
        """
        # boolean value
        if data.data != self.move_lock:
            rospy.loginfo("[{}/pose_controller]:\nMove lock engagement switched to {}".format(self.kobot_namespace,data.data))
        self.move_lock = data.data

    def pose_goal_callback(self, data):
        """
        For position control reference
        """
        pose = data.pose
        position = pose.position
        quaternion = pose.orientation
        self.goal_x = position.x
        self.goal_y = position.y
        explicit_quat = [
            quaternion.x, quaternion.y, quaternion.z, quaternion.w]
        eulers = tfc.transformations.euler_from_quaternion(explicit_quat)
        # planar rotation corresponds to eulers[2]
        self.goal_theta = eulers[2]
        self.err_th_prev = 0 
        self.err_th_sum = 0 
        self.err_th_diff = 0
        self.pos_err_prev = 0
        self.move2goal()
        # self.align2goal()

    def euclidean_distance(self, goal_x, goal_y):
        """
        Euclidean distance between current odom and the goal
        """
        return sqrt((goal_x - self.x)**2 +
                    (goal_y - self.y)**2)

    def angular_err(self, goal_theta):
        """
        Angle between current orientation and the goal
        """
        return (goal_theta - self.theta)

    def linear_vel(self, goal_x, goal_y):
        control_signal =  self.K_p_lin * self.euclidean_distance(goal_x, goal_y)
        if abs(control_signal) >= self.u_max: # prevent saturation for simulator control
            return self.u_max
        return control_signal

    def angular_vel_from_pos(self, goal_x, goal_y):
        """
        Calculate angular velocity needed for position goal
        """
        err_th = self.steering_angle(goal_x, goal_y) - self.theta
        err_th = wrap2pi(err_th)
        self.err_th_sum += err_th
        self.err_th_diff = (err_th - self.err_th_prev) / self.pose_control_freq

        control_signal = self.K_p_ang * err_th + self.K_d_ang * self.err_th_diff + self.K_i_ang * self.err_th_sum
        self.err_th_prev = deepcopy(err_th)
        if abs(control_signal) >= self.w_max: # prevent saturation for simulator control
            return self.w_max, err_th
        return control_signal, err_th

    def steering_angle(self, goal_x, goal_y):
        """
        Calculate angle needed for position goal
        """
        return atan2(goal_y - self.y, goal_x - self.x)

    def angular_vel_from_angle(self, goal_theta, constant=1.0):
        """
        Calculate angular velocity needed for orientation goal
        """
        return constant * (goal_theta - self.theta)

    def move2goal(self):
        """
        Moves robot to the goal postion x, y
        """
        vel_msg = Twist()
        penalty = 0
        iters = 0
        self.err_th_sum = 0
        self.err_th_diff = 0
        while not rospy.is_shutdown():
            pos_err = self.euclidean_distance(self.goal_x, self.goal_y)
            if pos_err <= self.distance_tolerance:
                break
            if pos_err > self.pos_err_prev:
                penalty += 1

            # linear velocity in the x-axis.
            vel_msg.linear.x = self.linear_vel(self.goal_x, self.goal_y)
            # vel_msg.linear.x = self.u_max
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0

            # angular velocity in the z-axis.
            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            angular_vel, err_th = self.angular_vel_from_pos(
                self.goal_x, self.goal_y)
            if self.angular_control_enabled:
                vel_msg.angular.z = angular_vel
            else:
                vel_msg.angular.z = 0

            if iters % self.report_interval == 0:
                rospy.loginfo("[{}/pose_controller]:\n\
                    Angular error: {:.6f}, Position error: {:.6f}".format(self.kobot_namespace,err_th/pi*180.0,pos_err))

            if self.move_lock or penalty > self.penalty_max:
                if self.move_lock:
                    rospy.loginfo(
                        "[{}/pose_controller]:\nMove lock, switching to velocity control".format(self.kobot_namespace))
                elif penalty > self.penalty_max:
                    rospy.loginfo(
                        "[{}/pose_controller]:\nControl penalties exceeded, switching to velocity control".format(self.kobot_namespace))
                bool_msg = Bool()
                bool_msg.data = False
                self.goal_reached_publisher.publish(bool_msg)
                rospy.loginfo("[{}/pose_controller]:\nGoal position not reached".format(self.kobot_namespace))
                return
            else:
                # publish vel_msg generated by pose controller
                self.velocity_publisher.publish(vel_msg)
            self.pos_err_prev = deepcopy(pos_err)
            # publish at the desired rate.
            self.odom_rate.sleep()
            # add a tick to the timer
            iters += 1

        # stopping our robot after the movement is over.
        vel_msg.linear.x = 0
        vel_msg.angular.z = 0
        self.velocity_publisher.publish(vel_msg)
        bool_msg = Bool()
        bool_msg.data = True
        self.goal_reached_publisher.publish(bool_msg)
        rospy.loginfo("[{}/pose_controller]:\nPosition goal reached".format(self.kobot_namespace))

    def align2goal(self):
        """
        Moves robot to the goal orientation theta
        """
        vel_msg = Twist()
        iters = 0
        while (not rospy.is_shutdown() and abs(self.angular_err(
                self.goal_theta)) >= self.angular_tolerance):
            if iters % self.report_interval == 0:
                rospy.loginfo("[{}/pose_controller]:\n{}".format(self.kobot_namespace,self.angular_err(self.goal_theta)))
            # Linear velocity in the x-axis.
            vel_msg.linear.x = 0
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0

            # Angular velocity in the z-axis.
            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            vel_msg.angular.z = self.angular_vel_from_angle(self.goal_theta)

            # Publishing our vel_msg
            self.velocity_publisher.publish(vel_msg)

            # Publish at the desired rate.
            self.odom_rate.sleep()

            # add a tick to the timer
            iters += 1

        # Stopping our robot after the movement is over.
        vel_msg.linear.x = 0
        vel_msg.angular.z = 0
        self.velocity_publisher.publish(vel_msg)

def wrap2pi(ang):
    """
    Returns given angle in 
    [-pi, +pi] 
    """
    ang = ang % (2*pi)
    if ang > pi:
        ang = pi - ang
        return -(pi + ang)
    elif ang < pi:
        return ang
    else:
        return -pi

if __name__ == '__main__':
    x = poseController()
    rate = rospy.Rate(x.pose_control_freq)
    while not rospy.is_shutdown():
        # update parameters, poll for update at given rate
        x.get_params()
        rate.sleep()

        #add a tick to the timer
        x.iters += 1
