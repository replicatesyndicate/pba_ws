#!/usr/bin/env python3
# Primary imports
import cv2
import numpy as np

# ROS Messages
from kobot_msgs.msg import landmark_pose
from std_msgs.msg import UInt8
from geometry_msgs.msg import TransformStamped, Vector3, Quaternion

# ROS imports
import rospy
# import tf
import tf2_ros as tf2
# import tf_conversions as tfc 
import tf as tfc #temporary until docker issues are resolved

# # for publishing dictionary as encoded string
# import json

class landmarkTfBroadcaster:
    def __init__(self):
        self.kobot_namespace=rospy.get_namespace().strip("/")
        # rospy.loginfo_once("kobot_namespace: {}".format(self.kobot_namespace)) #check your namespace here
        
        # initialize parameters
        self.get_params()

        # pub landmark ID to wake up subscriber for landmark tf
        self.landmark_pub = rospy.Publisher("sensors/landmark_id",
            UInt8, queue_size=1)
        self.broadcaster = tf2.TransformBroadcaster()
        self.tfBuffer = tf2.Buffer(self.buffering_duration)
        self.listener = tf2.TransformListener(self.tfBuffer)
        self.landmark_tf_dict = {}

        self.rt_sub = rospy.Subscriber("sensors/landmark",
            landmark_pose, self.rt_callback, queue_size=1)

        #rate limiters    
        self.prev_republish_time = None

    def rt_callback(self, data):
        """
        Get rvec and tvec from aruco detector's published msg
        send tf b/w cam_link and detected aruco
        """
        # rospy.loginfo("Landmark data:{}".format(data))
        # rospy.loginfo("Encountered landmark! landmark_{}".format(data.id))
        # we need a homogeneous matrix but 
        # OpenCV only gives us a 3x3 rotation matrix
        rotation_matrix = np.array([[0, 0, 0, 0],
                                    [0, 0, 0, 0],
                                    [0, 0, 0, 0],
                                    [0, 0, 0, 1]],
                                    dtype=float)

        rotation_matrix[:3, :3], _ = cv2.Rodrigues(data.rvec)

        # convert the matrix to euler angles 
        euler = tfc.transformations.euler_from_matrix(rotation_matrix, 'rxyz')
        if abs(euler[1]) > self.landmark_max_ang:
            # we are looking from a problematic angle, ignore it
            return

        # euler angle sequencing -- keep landmarks coplanar with map, but with a Z axis angular offset:
        # default camera orientation requires a <-90,0,-90> (rxyz) rotation, 
        #   the corresponding (ryxz) is <90,90,0>. 
        quaternion = tfc.transformations.quaternion_from_euler(
            self.default_ori[0],
            self.default_ori[1],
            self.default_ori[2] + euler[1],
            'ryxz') 
        
        current_time = rospy.Time.now()
        # TF2 version
        t = TransformStamped()
        t.header.stamp = current_time
        t.header.frame_id = "cam_link_{}".format(self.kobot_namespace)
        t.child_frame_id = "landmark{}_{}".format(data.id,self.kobot_namespace)

        # # NOTE: previous version - fixed to cam_link
        # # NOTE: modified for melodic - can revert to star operator if noetic is used
        # t.transform.translation = Vector3(*data.tvec)
        # t.transform.rotation = Quaternion(*quaternion)

        # NOTE: previous version - fixed to cam_link
        # tvec is the translation from cam_link to landmark
        t.transform.translation.x = data.tvec[0]
        t.transform.translation.y = data.tvec[1]
        t.transform.translation.z = data.tvec[2]
        t.transform.rotation = Quaternion(quaternion[0],quaternion[1],quaternion[2],quaternion[3])

        # beta = data.rvec[1] - np.pi # rotation of base_link with respect to line segment from robot to landmark center
        # dist = data.tvec[2]         # displacement of robot from landmark

        # # position of landmark with respect to cam_link frame, reminder that base_link frame <x,y> are cam_link <z,-x>
        # t.transform.translation.x = -dist*np.sin(beta)
        # t.transform.translation.y = 0
        # t.transform.translation.z = dist*np.cos(beta)
        
        #NOTE: modified for melodic - can revert to star operator if noetic is used
        # self.landmark_tf_dict[data.id] = [[*data.tvec],[*quaternion]]
        self.landmark_tf_dict[data.id] = [[t.transform.translation.x,t.transform.translation.y,t.transform.translation.z], 
        [t.transform.rotation.x,t.transform.rotation.y,t.transform.rotation.z,t.transform.rotation.w]]
        
        # print transform data
        rospy.loginfo("Transform in cam_link (i.e. x -> -y, z -> x): {}".format(t))

        # publish landmark to lba driver
        landmark_id_msg = UInt8()
        landmark_id_msg = data.id
        self.landmark_pub.publish(landmark_id_msg)

    def landmark_broadcast(self, republish=False):
        """
        Broadcast latest known landmarks at a rate determined by this script 
        so that the TF chain leading to the landmark frames is not broadcast too rapidly.\n

        The rationale for this distinction is that the sensor callbacks from the simulation
        may not be rate-limited, and therefore overload the TF module if the transforms are made
        directly within the callback above.\n

        Do nothing if no landmark has been seen within an iteration.\n

        After the broadcast, clear out the buffer if republishing is turned off. 
        Otherwise only update the dictionary through the callback, and keep the data to 
        make landmarks persistent in the TF chain.\n

        NOTE: Expecting this call to not break real robot scenarios. Delete this line when testing is complete.\n
        """
        #rospy.loginfo("Republishing landmarks.")
        current_time = rospy.Time.now()
        # guard against repeated TF calls within short windows of time for better performance
        # i.e. check against the Nyquist frequency for available tvec/rvec data.
        if self.prev_republish_time is None or current_time - self.prev_republish_time > rospy.Duration(0.5/self.landmark_tf_freq):
            try:  
                for key,val in self.landmark_tf_dict.items():       
                    # TF2 version
                    t = TransformStamped()
                    t.header.stamp = current_time
                    t.header.frame_id = "cam_link_{}".format(self.kobot_namespace)
                    t.child_frame_id = "landmark{}_{}".format(key,self.kobot_namespace)
                    #NOTE: modified for melodic - can revert to star operator if noetic is used
                    # t.transform.translation = Vector3(*val[0])
                    # t.transform.rotation = Quaternion(*val[1])
                    t.transform.translation = Vector3(val[0][0],val[0][1],val[0][2])
                    t.transform.rotation = Quaternion(val[1][0],val[1][1],val[1][2],val[1][3])
                    # rospy.loginfo_once("Republish format: {}".format(t))
                    self.broadcaster.sendTransform(t)
                self.prev_republish_time = current_time

                if republish is False: # empty out broadcasting dictionary if republishing is off
                    self.landmark_tf_dict = {} 

            #dictionary size changed while iterating (rare)
            except RuntimeError:
                #try it again later, no big deal
                pass

    def get_params(self):
        """
        LBA params. are checked constantly and is
        updated if necessary
        """
        if rospy.has_param('landmark_tf'):
            params = rospy.get_param('landmark_tf')
            self.tf_world_frame = params['tf_world_frame']
            self.landmark_tf_freq = params['landmark_tf_freq']
            self.landmark_max_ang = params['landmark_max_ang']
            self.default_ori = np.deg2rad(params['default_sensor_orientation'])
            self.buffering_duration = rospy.Duration(params['buffering_duration'])

        else:
            self.tf_world_frame = "vicon/world"
            self.landmark_tf_freq = 15
            self.landmark_max_ang = 1.0
            # self.default_ori = [0.0,0.0,0.0] # for simulated environments
            self.default_ori = [np.pi/2,np.pi/2,0.0] # for real robots
            self.buffering_duration = rospy.Duration(6.0)
    # def repub_landmarks(self):
    #     """
    #     Broadcast latest known landmarks at a rate so that the TF chain leading to
    #     the landmark frames is not forgotten.
    #     This is entirely optional, if landmark frame data persistence is not required,
    #     the user can comment out the method call in start().
    #     """
    #     #rospy.loginfo("Republishing landmarks.")
    #     current_time = rospy.Time.now()
    #     # guard against repeated TF calls within short windows of time for better performance
    #     # i.e. check against the Nyquist frequency of available tvec/rvec data.
    #     if self.prev_republish_time is None or current_time - self.prev_republish_time > rospy.Duration(0.5/self.landmark_tf_freq):
    #         try:  
    #             for key,val in self.landmark_tf_dict.items():       
    #                 # TF2 version
    #                 t = TransformStamped()
    #                 t.header.stamp = current_time
    #                 t.header.frame_id = "cam_link_{}".format(self.kobot_namespace)
    #                 t.child_frame_id = "landmark{}_{}".format(key,self.kobot_namespace)
    #                 #NOTE: modified for melodic - can revert to star operator if noetic is used
    #                 # t.transform.translation = Vector3(*val[0])
    #                 # t.transform.rotation = Quaternion(*val[1])
    #                 t.transform.translation = Vector3(val[0][0],val[0][1],val[0][2])
    #                 t.transform.rotation = Quaternion(val[1][0],val[1][1],val[1][2],val[1][3])
    #                 # rospy.loginfo_once("Republish format: {}".format(t))
    #                 self.broadcaster.sendTransform(t)
    #             self.prev_republish_time = current_time
    #         #dictionary size changed while iterating (rare)
    #         except RuntimeError:
    #             #try it again later, no big deal
    #             pass

def lookup_tf(buffer, target_frame, source_frame, duration = 0.1):
    """
    Modified for TF2.\n
    Get the latest available tf as translation and rotation vectors from the tf stack between the given target and source frames.\n
    If source frame origin is located at (x,y,z) away from the target frame's origin, (x,y,z) will be returned as the translation.\n
    If target frame is rotated (x,y,z) CCW about each axis to get the orientation of the target frame in space, (x,y,z) will be returned as the orientation.
    """
    try:
        #of type geometry_msgs/Transform 
        t = buffer.lookup_transform(target_frame, source_frame, rospy.Time(0), rospy.Duration(duration))
        trans = (t.transform.translation.x, t.transform.translation.y, t.transform.translation.z)
        rot = (t.transform.rotation.x, t.transform.rotation.y, t.transform.rotation.z,t.transform.rotation.w)
        return [trans,rot]
    except (tf2.LookupException, tf2.ConnectivityException,
            tf2.ExtrapolationException):
        rospy.logerr("TF from {} to {} N/A landmark TF".format(
            target_frame,
            source_frame))  

def start():
    rospy.init_node('landmark_tf', anonymous=True)

    ltf = landmarkTfBroadcaster()
    rate = rospy.Rate(ltf.landmark_tf_freq)
    while not rospy.is_shutdown():
        ltf.get_params()
        ltf.landmark_broadcast(republish = False)
        rate.sleep()

if __name__ == '__main__':
        start()
        