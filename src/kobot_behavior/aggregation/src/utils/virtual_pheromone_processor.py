#!/usr/bin/env python3
# Primary imports
import numpy as np
import os, sys
#ROS imports
import rospy
#ROS standard messages
# TODO: add Vicon position datatype for real robot experiments
from geometry_msgs.msg import Pose2D,Point
from std_msgs.msg import Float64
from kobot_msgs.msg import pheromone_sensor, pheromone_grid

class NodeHandle:
  """
  Virtual pheromone emission and sensing kit.
  Tracks all robots and feeds them back the current pheromone readouts.
  Handles emissions of pheromones when requested by robots.

  Preferred ROS node ID/NS: /pheromone
  """
  def __init__(self):
    # Access parameter server to get count of robots and arena information
    self.get_params()
    # Initialize grid parameters
    self.init_grid()
    # Subscriber and publisher lists
    self.pose_subs = []
    self.emitter_subs = []
    self.pheromone_pubs = []

    # Attribute initialization
    ## Callback data
    self.robot_pose = [Pose2D()] * self.robot_count
    self.emission_requests = [0.0] * self.robot_count
    ## Rate setup from parameters
    self.evaporation = 1 - (1-self.evaporation_rate)**(1/self.frequency)

    # Initialize subscribers
    for id in range(self.robot_count):
      # TODO: implement a Vicon topic tracker and an if-else switch
      self.pose_subs.append(rospy.Subscriber(name= "/{}{}/{}".format(self.robot_ns,id,self.pose_name),
                          data_class= Pose2D,
                          callback= self.pose_callback,
                          callback_args= id,
                          queue_size= 1))
      self.emitter_subs.append(rospy.Subscriber(name= "/{}{}/{}".format(self.robot_ns,id,self.subscriber_name),
                          data_class= Float64,
                          callback= self.emitter_callback,
                          callback_args= id,
                          queue_size= 1))
    # Initialize pheromone_pubs
    for id in range(self.robot_count):
      self.pheromone_pubs.append(rospy.Publisher(name= "/{}{}/{}".format(self.robot_ns,id,self.publisher_name),
                                                   data_class= pheromone_sensor,
                                                   queue_size= 1))
    self.pheromone_data_pub = rospy.Publisher(name="/pheromone/data",
                                                  data_class= pheromone_grid,
                                                  queue_size= 10
                                                  )

  def get_params(self):
    """
    Get parameters for simulation or robot trials
    by default checks sim_params_aggregation.yaml and pheromone.yml or any identical parameter set

    You should introduce these parameters privately,
    or through a rosbash API call where the ns is set as "/pheromone".

    /pheromone/sim, /pheromone/swarm, /pheromone/node /pheromone/floor/outer_radius_of_floor refer to sim_params_aggregation.yaml

    /pheromone/frequency, /pheromone/publisher, /pheromone/subscriber,
    /pheromone/robot_count, /pheromone/pose_topic,
    /pheromone/robot_radius, /pheromone/arena server access namespaces refer to pheromone.yml
    """

    # unique to pheromone.yml
    self.robot_ns = rospy.get_param('/pheromone/robot_ns', default= "kobot")
    self.frequency = rospy.get_param('/pheromone/frequency', default= 120)
    self.evaporation_rate = rospy.get_param('/pheromone/evaporation_rate', default= 0.1)
    self.static_grid_path = rospy.get_param('/pheromone/static_grid_path', default= None)
    self.dynamic_grid_path = rospy.get_param('/pheromone/dynamic_grid_path', default= None)
    self.sensor_type = rospy.get_param('/pheromone/sensor_type', default="weighted_average")
    if self.sensor_type not in ("weighted_average", "instance"):
      rospy.logerr('[virtual_pheromone_processor.py]: Invalid sensor_type, defaulting to "weighted_average"')
      self.sensor_type = "weighted_average"

    saturation = rospy.get_param('/pheromone/saturation', default= "np.Inf") # clip high values
    if saturation.casefold() == "inf":
      saturation = "np.Inf"
    try:
      if type(eval(saturation)) is not float:
        self.saturation = np.Inf
      else:
        self.saturation = eval(saturation)
    except (TypeError, ValueError, NameError):
      rospy.logerr("[virtual_pheromone_processor]: Saturation parameter invalid, not applying.")
      self.saturation = np.Inf

    self.publisher_name = rospy.get_param('/pheromone/publisher', default= 'sensors/pheromone')
    self.subscriber_name = rospy.get_param('/pheromone/subscriber', default= 'pheromone_emitter')

    self.grid_resolution = rospy.get_param('/pheromone/resolution', default= 0.02) # defaults to a third of robot radius in meters
    injection_radius = rospy.get_param('/pheromone/injection_radius', default = 0.08) # injection radius in meters
    self.injection_radius = injection_radius / self.grid_resolution # safe to have floats here
    # injection position in mm, in robot body centered frame
    self.injector_pos_x = rospy.get_param('/pheromone/injector_pos_x', default= 0.0) / 1000 # [mm] towards the front
    self.injector_pos_y = rospy.get_param('/pheromone/injector_pos_y', default= 0.0) / 1000 # [mm] towards the left

    self.diffusion_coefficient = rospy.get_param('/pheromone/diffusion_coefficient', default= 1) # how strongly the diffusion occurs (0-inf)
    self.cue_intensity = rospy.get_param('/pheromone/cue_intensity', default=50) # intensity of pheromones over cue area

    self.diffusion_enabled = rospy.get_param('/pheromone/diffusion', default= False) # enables diffusion
    self.evaporation_enabled = rospy.get_param('/pheromone/evaporation', default= False) # enables evaporation

    if rospy.has_param('/pheromone/sensor_positions'):
      # placement of sensors (in row-major ordering), mm
      sensor_positions = rospy.get_param('/pheromone/sensor_positions')
      self.sensor_positions = np.asarray(sensor_positions) / 1000
    else: # default
      self.sensor_positions = np.array([
                      [25,-25],
                      [25,0],
                      [25,25],
                      [0,-25],
                      [0,0],
                      [0,25],
                      [-25,-25],
                      [-25,0],
                      [-25,25]
                      ]) / 1000

    if rospy.has_param('/pheromone/sim'):
      # expecting inputs scaled to millimeters, outputs in meters
      width_pixels = rospy.get_param('/pheromone/sim/width', default=420)
      height_pixels = rospy.get_param('/pheromone/sim/height', default=420)
      pixel_p_mm = rospy.get_param('/pheromone/sim/pixel_p_mm', default=0.15)
      self.tf_parent_frame = rospy.get_param('/pheromone/sim/tf_parent_frame', default='vicon/world')

      self.arena_width = width_pixels / pixel_p_mm / 1000
      self.arena_height = height_pixels / pixel_p_mm / 1000
    elif rospy.has_param('/pheromone/arena'):
      # expecting inputs in meters
      self.arena_width = rospy.get_param('/pheromone/arena/width', default=2.8)
      self.arena_height = rospy.get_param('/pheromone/arena/height', default=2.8)
      self.tf_parent_frame = rospy.get_param('/pheromone/arena/tf_parent_frame', default='vicon/world')

    if rospy.has_param('/pheromone/swarm'):
      self.robot_count = rospy.get_param('/pheromone/swarm/num_of_drone', default=4)
    elif rospy.has_param('/pheromone/robot_count'):
      self.robot_count = rospy.get_param('/pheromone/robot_count', default=4)
    else:
      rospy.logwarn("[virtual_pheromone_processor.py]: Robot count unspecified. Defaulting to 4.")
      self.robot_count = 4

    if rospy.has_param('/pheromone/node'):
      self.pose_name = rospy.get_param('/pheromone/node/pose_topic')
    elif rospy.has_param('/pheromone/pose_topic'):
      self.pose_name = rospy.get_param('/pheromone/pose_topic')
    else:
      # not expecting the topic name to change too frequently
      self.pose_name = 'pose2d_global'

    self.save_to_file = rospy.get_param("/pheromone/save_to_file", default= False)

  def init_grid(self):
    """
    Initialize necessary parameters for the visual and functional grid data pertaining to the pheromone emissions and detections.
    Generates grids with indexing that is akin to NumPy arrays (row-by-column; left-to-right, top-to-bottom)
    Grid cell sizing is done in a way that exactly squares the circular footprint of a robot.
    Pheromone injections on this grid will then be done by a kernel addition (4-connective, 8-connective,etc.)

    NOTE: The user is responsible of keeping the cell count limit to be in the uint32 range (fairly high).
    """
    # NOTE: the final row and column may exceed the arena borders, snap_to_grid() however works the same way
    width_cells = int(np.ceil(self.arena_width/self.grid_resolution)) + 1 # to prevent undercutting add 1
    height_cells = int(np.ceil(self.arena_height/self.grid_resolution)) + 1 # to prevent undercutting add 1
    origin_point = Point(x= -self.arena_width/2, y= -self.arena_height/2 ,z= 0)

    # NOTE: the pheromone grid follows from the top left cell, towards the right and the bottom for indexing. a[0][1] declares the 0th row, 1st column, from the top left.
    self.pheromone_grid = np.zeros((height_cells,width_cells)) # empty grid

    # Fetch pheromones from file - pass in your filename in pheromone.yml. Expects a file "grids/*.np[y/z]".
    cwd = os.path.abspath(os.path.dirname(sys.argv[0]))
    try:
      path = f"{cwd}/grids/{self.static_grid_path}"
      sgrid = np.load(path)
      # shape checks to match config prescriptions
      if sgrid.shape == (height_cells,width_cells):
        self.static_grid = sgrid
        rospy.loginfo(f"Loaded static grid: {path}")
      else:
        rospy.logwarn(f"Static grid of shape {sgrid.shape} does not match ({height_cells},{width_cells}). Overlaying empty grid.")
        self.static_grid = np.zeros_like(self.pheromone_grid)
    except (ValueError, FileNotFoundError, TypeError, IsADirectoryError):
      self.static_grid = np.zeros_like(self.pheromone_grid)
      rospy.loginfo(f"No valid static grid file provided. Overlaying empty grid.")

    try:
      path = f"{cwd}/grids/{self.dynamic_grid_path}"
      pgrid = np.load(path)
      # shape checks to match config prescriptions
      if pgrid.shape == (height_cells,width_cells):
        self.pheromone_grid = pgrid
        rospy.loginfo(f"Loaded dynamic grid: {path}")
      else:
        rospy.logwarn(f"Dynamic grid of shape {pgrid.shape} does not match ({height_cells},{width_cells}). Overlaying empty grid.")
    except (ValueError, FileNotFoundError, TypeError, IsADirectoryError):
      rospy.loginfo(f"No valid dynamic grid file provided. Overlaying empty grid.")

    # # DEBUG: place some pheromones on the grid, these are manual so be careful
    # self.pheromone_grid[30:51,30:51] += 1000
    # self.pheromone_grid[630:651,30:51] += 1000
    # self.pheromone_grid[630:651,1000:1021] += 1000

    # # DEBUG: cells with increasing intensity in row-major order, test formatting, sensing, visualization, and evaporation; manual again
    # self.pheromone_grid = np.arange(0,width_cells*height_cells).reshape((height_cells,width_cells)).astype(np.float64) / 200

    rospy.logdebug("Expected shape: {}, {}".format(height_cells,width_cells))
    rospy.logdebug("Pheromone grid shape: {}".format(self.pheromone_grid.shape))

    # place pheromones that stay constant during runtime
    self.generate_static_area()

  # Callbacks and publisher topic formatter functions
  def pose_callback(self,msg,id):
    self.robot_pose[id] = msg
  def emitter_callback(self,msg,id):
    self.emission_requests[id] = msg.data
  def grid_data_topic(self,grid):
    msg = pheromone_grid()
    msg.grid = grid.ravel()
    msg.height = grid.shape[0]
    msg.width = grid.shape[1]
    self.pheromone_data_pub.publish(msg)

  # Functional methods
  def generate_static_area(self):
    """
    Load static area from a file and overlay it on the pheromone grid.
    The user can also save the currently laid pheromones by enabling the self.save_to_file flag.
    The user can use the method generate_cue_area() as well to place a circular pheromonal area on the grid at a declared position.
    It's recommended to turn off diffusion and evaporation while generating static areas using the processor to avoid bleeding and data loss.
    """
    self.total_grid = np.where(self.static_grid > 0, self.static_grid, self.pheromone_grid)

  def generate_cue_area(self):
    """
    Generate static pheromones, typically representing the cue area.
    Required parameters are: Cue radius, grid resolution, cue (pheromone) intensity.
    """
    # define mask for circular region (clips when out of bounds)
    self.total_grid = self.pheromone_grid.copy()
    x = np.arange(0,self.total_grid.shape[1])
    y = np.arange(0,self.total_grid.shape[0])
    mask = (x[np.newaxis,:]-self.total_grid.shape[1]/2)**2 + (y[:,np.newaxis]-self.total_grid.shape[0]/2)**2 < (self.outer_cue_radius/self.grid_resolution)**2
    self.total_grid[mask] = self.cue_intensity

  def snap_to_grid(self,x,y):
    """
    Snap a robot's position to a grid.
    Inputs:
    - x,y: Global robot position in meters (horizontal, vertical).
    Outputs:
    - i,j: Robot position in grid indices (horizontal, vertical).
    """
    # the edge case of being exactly on the grid lines is not an expected case. if it somehow happens, the snapping is done towards the right side or the bottom side of the robot.
    i = int((x + self.arena_width/2) / self.grid_resolution)
    j = int((-y + self.arena_height/2) / self.grid_resolution)
    return i,j

  def diffuse(self, stdev = 1):
    """
    Apply diffusion operation on the pheromone grid.
    Uses Gaussian blur by default to apply diffusion.
    Expects a 2-D kernel normalized to a sum of 1, to then scale it.

    Inputs:
    - stdev: Standard deviation for Gaussian blur kernel
    """
    # since discrete approximation is used for Gaussian blurring, the pheromone energy will NOT be constant as expected
    total_pheromone_energy = np.sum(self.pheromone_grid)

    ax = np.array([-1.,0.,1.])
    kernel = np.exp(-0.5 * np.square(ax) / np.square(stdev)) / (stdev * np.sqrt(2*np.pi))

    self.pheromone_grid = decomposed_conv2d(self.pheromone_grid,kernel,kernel)
    if np.sum(self.pheromone_grid) != 0.0:
      # rescale to preserve total pheromone energy
      self.pheromone_grid *= (total_pheromone_energy/np.sum(self.pheromone_grid))
  def inject(self):
    """
    Apply requested pheromone emissions from all robots to the pheromone grid.
    The request durations will diminish over time, and if no new requests are issued they will naturally expire.
    If a request is overwritten through a callback routine, the new request is applied.
    """
    for id in range(self.robot_count):
      if self.emission_requests[id] == 0.0:
        continue
      # this robot is emitting pheromones
      # get injector position
      inj_x = self.robot_pose[id].x + self.injector_pos_x*np.cos(self.robot_pose[id].theta) - self.injector_pos_y*np.sin(self.robot_pose[id].theta)
      inj_y = self.robot_pose[id].y + self.injector_pos_x*np.sin(self.robot_pose[id].theta) + self.injector_pos_y*np.cos(self.robot_pose[id].theta)
      i,j = self.snap_to_grid(inj_x,inj_y)
      # get lists of indices to run distance comparison with
      x = np.arange(0,self.pheromone_grid.shape[1])
      y = np.arange(0,self.pheromone_grid.shape[0])
      # define mask for circular region (clips when out of bounds)
      mask = (x[np.newaxis,:]-i)**2 + (y[:,np.newaxis]-j)**2 < self.injection_radius**2
      self.pheromone_grid[mask] += self.emission_requests[id]/self.frequency

  def neighbor_check(self,sensor_pos):
    """
    Snap sensor position as a floating index value, then get the indices that
    box in the sensor.
    """
    # "floating index" values are at most +- 0.5 units away from a cell center
    j = (sensor_pos[0] + self.arena_width/2) / self.grid_resolution -0.5 # horz
    i = (-sensor_pos[1] + self.arena_height/2) / self.grid_resolution -0.5 # vert
    cj,ci = self.snap_to_grid(sensor_pos[0],sensor_pos[1]) # snapped cell center
    # check quadrants, right-hand side is positive, bottom-hand side is positive (row-major ordering)
    round_i = np.sign(i-ci) + ci
    round_j = np.sign(j-cj) + cj

    if 0 <= round_i < self.total_grid.shape[0] and 0 <= round_j < self.total_grid.shape[1]:
      return (i,j), np.array([(ci,cj), (ci,round_j), (round_i,cj), (round_i,round_j)], dtype=int)
    if 0 <= round_j < self.total_grid.shape[1]:
      return (i,j), np.array([(ci,cj), (ci,round_j)], dtype=int)
    if 0 <= round_i < self.total_grid.shape[0]:
      return (i,j), np.array([(ci,cj), (round_i,cj)], dtype=int)
    return (i,j), np.array([(ci,cj)])

  def weighted_sensor_readout(self,sensor_pos):
    """
    Detect sensor readout for one simulated sensor, at given position and index
    """
    try:
      float_idx, neighbor_idx = self.neighbor_check(sensor_pos)
      intensities = self.total_grid[neighbor_idx[:,0],neighbor_idx[:,1]]
      distances = np.linalg.norm(neighbor_idx - float_idx, axis=1)
      inv_dist = 1 / distances
      readout = np.sum(intensities * inv_dist) / np.sum(inv_dist)
      return readout
    except:
      return 0.0

  def detect_pheromones(self):
    """
    Publish current pheromone readouts for all robots in their own topics.
    The front end of the robot will always point towards (0,1) in the sensor kernel.
    """
    for robot_id in range(self.robot_count):
      sensor_positions = self.sensor_positions.copy()
      center_pos = np.array([self.robot_pose[robot_id].x,self.robot_pose[robot_id].y])
      sensor_ang = -np.pi/2 + self.robot_pose[robot_id].theta
      rot = np.array([[np.cos(sensor_ang), -np.sin(sensor_ang)],[np.sin(sensor_ang), np.cos(sensor_ang)]])
      rotated_sensor_positions = center_pos + (np.dot(rot,sensor_positions.T)).T
      
      intensity_readouts = np.zeros(9).astype(np.float64)
      # previously took sensor_positions instead and did calcs per iteration
      for sensor_id, sensor_pos in enumerate(rotated_sensor_positions):
        # mag = np.linalg.norm(sensor_pos)
        # ang = np.arctan2(sensor_pos[1],sensor_pos[0])
        # sensor_pos[0] = self.robot_pose[id].x + mag * np.cos(ang + sensor_ang)
        # sensor_pos[1] = self.robot_pose[id].y + mag * np.sin(ang + sensor_ang)
        # pos = [
        #   self.robot_pose[robot_id].x + np.cos(sensor_ang)*sensor_pos[0] - np.sin(sensor_ang)*sensor_pos[1],
        #   self.robot_pose[robot_id].y + np.sin(sensor_ang)*sensor_pos[0] + np.cos(sensor_ang)*sensor_pos[1]
        # ]
        if self.sensor_type == "instance":
          # get values directly from cells
          j, i = self.snap_to_grid(sensor_pos[0],sensor_pos[1])
          # prevent clipping
          if 0 <= i <= self.total_grid.shape[1] or 0 <= j <= self.total_grid.shape[0]:
            # indices within arena read values
            intensity_readouts[sensor_id] = self.total_grid[i,j]
        elif self.sensor_type == "weighted_average":
                  # use weighted values instead
                  intensity_readouts[sensor_id] = self.weighted_sensor_readout(sensor_pos)
      # rospy.loginfo("Readouts:\n{}".format(intensity_readouts.reshape((3,3))))
      msg = pheromone_sensor(readouts= intensity_readouts)
      self.pheromone_pubs[robot_id].publish(msg)

## Helper functions
def decomposed_conv2d(arr,x_kernel,y_kernel):
  """
  Apply two 1D kernels as a part of a 2D convolution.
  The kernels must be the decomposed from a 2D kernel
  that originally is intended to be convolved with the array.
  Inputs:
  - x_kernel: Column vector kernel, to be applied along the x axis (axis 0)
  - y_kernel: Row vector kernel, to be applied along the y axis (axis 1)
  """
  arr = np.apply_along_axis(lambda x: np.convolve(x, x_kernel, mode='same'), 0, arr)
  arr = np.apply_along_axis(lambda x: np.convolve(x, y_kernel, mode='same'), 1, arr)
  return arr

## Main function
def start():
  """
  Main function for the script.
  See <class: NodeHandle> for more details.
  """
  # Initialize rospy node and rate, along with the script object
  rospy.init_node("pheromone")
  nh = NodeHandle()
  rate = rospy.Rate(nh.frequency)
  # Main loop
  while not rospy.is_shutdown():
    # 1. Report current values
    ## detect pheromones and publish to all robot units as a stand-in for pheromone sensors
    nh.detect_pheromones()
    nh.grid_data_topic(nh.total_grid)

    # 2. Inject emissions to update pheromone field
    nh.inject()

    # 3. Diffuse pheromones that are on the pheromone field
    if nh.diffusion_enabled:
      nh.diffuse(nh.diffusion_coefficient)

    # 4. Evaporate pheromones by the given evaporation rate (roughly equates to fractional loss per second)
    if nh.evaporation_enabled:
      nh.pheromone_grid *= (1 - nh.evaporation)

    ## clip values that have turned negative or gone above saturation
    nh.pheromone_grid = np.clip(nh.pheromone_grid, 0.0, nh.saturation)

    ## generate the "total pheromone field", by adding static pheromones
    ## if no static field is provided, an empty overlay will be added (i.e. nothing happens)
    nh.generate_static_area()

    ## save to file
    if nh.save_to_file:
      np.save("vpg.npy", nh.total_grid)

    rate.sleep()

if __name__ == "__main__":
  start()