#!/usr/bin/env python3
# Primary imports
import numpy as np
#ROS imports
import rospy
#ROS standard messages
# TODO: add Vicon position datatype for real robot experiments
from geometry_msgs.msg import Pose2D,Point,Pose,Quaternion
from std_msgs.msg import Float64, Bool
from nav_msgs.msg import OccupancyGrid, MapMetaData
from kobot_msgs.msg import pheromone_sensor

class Injector:
    """
    Set up pheromone injection debugging tools.
    
    Preferred ROS node ID/NS: /pheromone
    """
    def __init__(self):      
        self.request_sub = rospy.Subscriber(name= "injection_request",
                                            data_class= Bool,
                                            callback= self.request_callback,
                                            queue_size= 1)
        self.injector_pub = rospy.Publisher(name= "/kobot0/pheromone_emitter",
                                            data_class= Float64,
                                            queue_size= 1)

        # Attribute initialization
        ## Parameters - Enter manually
        self.frequency = 10
        self.intensity = 15
        modifier = 0.1
        self.diminishing_rate = 1 - (1 - modifier)**(1/self.frequency)

        ## Variables
        self.injection_value = 0.0

    # Callbacks and publisher topic formatter functions
    def request_callback(self,msg):
        if msg.data == True:
            rospy.logwarn("Laying pheromones")
            self.injection_value = self.intensity
        else:
            rospy.logwarn("Stopping")
            self.injection_value = 0.0

## Main function
def start():
    """
    Main function for the script.
    """
    # Initialize rospy node and rate, along with the script object
    rospy.init_node("debug_pheromone_injector")
    inj = Injector()
    rate = rospy.Rate(inj.frequency)
    cnt = 0
    # Main loop
    while not rospy.is_shutdown():
        cnt += 1
        inj.injection_value *= (1- inj.diminishing_rate)
        inj.injector_pub.publish(inj.injection_value)
        if cnt % 10 == 0:
            rospy.loginfo("Value: {}".format(inj.injection_value))
        rate.sleep()

if __name__ == "__main__":
    start()