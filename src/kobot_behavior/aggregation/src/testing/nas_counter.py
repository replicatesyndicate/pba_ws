#!/usr/bin/env python
from datetime import datetime
import rospy
from geometry_msgs.msg import Pose2D
import numpy as np

class Counter(object):
    """
    Add subscribers to global pose data for each Kobot unit, and attach their callbacks to collect NAS data_collection for aggregation.
    Assumes that the cue area center is the origin of the world frame (i.e. center of cue area circle is at (0,0)).
    For other arena geometries, modify the callback function.
    """
    def __init__(self):
        self.get_params()
        self.datetime = datetime.now().strftime('%d_%m_%Y_%H_%M_%S')
        self.filename_ext = self.filename + '_' + self.datetime
        rospy.loginfo("Rate: {}, Radius: {}, Count: {}, Topic: {}, Filename: {}".format(self.frequency,self.radius,self.kobot_count,self.topic_name,self.filename_ext))
   
        self.nas = []
        self.time = []
        self.k = [0]*self.kobot_count

        self.subscribers = []

        for id in range(self.kobot_count):
            self.subscribers.append(rospy.Subscriber(name= "/kobot{}/{}".format(id,self.topic_name), 
                                                     data_class= Pose2D,
                                                     callback= self.pose2d_callback, 
                                                     callback_args= id,
                                                     queue_size=1))
    
    def get_params(self):
        """
        Get parameters for simulation or robot trials
        by default checks sim_params_aggregation.yaml or any identical parameter set
        /nas_counter/floor, /nas_counter/swarm, and /nas_counter/node refer to sim_params_aggregation.yaml
        /nas_counter_* parameters refer to individual private param tags for this script (add them to your launch file if they aren't provided by a yaml)
        """
        
        self.frequency = rospy.get_param('/nas_counter/data_collection/frequency',default=120)

        if rospy.has_param('/nas_counter/floor'): # in millimeters!
            self.radius = rospy.get_param('/nas_counter/floor')["outer_radius_of_floor"] / 1000
        elif rospy.has_param('/nas_counter/nas_counter_radius'):
            # alternative parameter naming for individual launching
            self.radius = rospy.get_param('/nas_counter/nas_counter_radius') / 1000
        # rospy.loginfo("Radius: {}".format(self.radius))

        if rospy.has_param('/nas_counter/swarm'):
            self.kobot_count = rospy.get_param('/nas_counter/swarm')["num_of_drone"]
        elif rospy.has_param('/nas_counter/nas_counter_count'):
            # alternative parameter naming for individual launching
            self.kobot_count = rospy.get_param('/nas_counter/nas_counter_count')
        # rospy.loginfo("Count: {}".format(self.kobot_count))

        if rospy.has_param('/nas_counter/node'):
            self.topic_name = rospy.get_param('/nas_counter/node')["pose_topic"]
        elif rospy.has_param('/nas_counter/nas_counter_pose_topic'):
            # alternative parameter naming for individual launching
            self.topic_name = rospy.get_param('/nas_counter/nas_counter_pose_topic')
        else:
            # not expecting the topic name to change too frequently
            self.topic_name = 'pose2d_global'

        if rospy.has_param('/nas_counter/data_collection/filename'):
            self.filename = rospy.get_param('/nas_counter/data_collection/filename')
            rospy.loginfo("Experiment name received. The file will be saved as {}.npy".format(self.filename))
        else:
            self.filename = "test"
            rospy.loginfo("No experiment name given. The file will be saved as test.npy")

        if (rospy.has_param('/nas_counter/floor') == True and rospy.has_param('/nas_counter/swarm') == True) or (rospy.has_param('/nas_counter/nas_counter_radius') == True and rospy.has_param('/nas_counter/nas_counter_count') == True):
            pass
        else:
            rospy.logerr("Parameters not accessed. Check your launch file.")   
            raise

    def pose2d_callback(self,data,id):
        dist = np.sqrt(np.square(data.x) + np.square(data.y))
        if dist < self.radius:
            self.k[id]=1
            # rospy.logwarn("k{}".format(id))
        elif dist > self.radius:
            self.k[id]=0
        
    def nas_val(self):		
        nas_val = float(sum(self.k))/float(self.kobot_count)
        self.nas.append(nas_val)
        self.time.append(rospy.Time.now().to_sec())
        print("Current NAS value: {}".format(nas_val))
        print("Current readouts: {}".format(self.k))

def main():
    rospy.init_node("nas_counter", anonymous=False)
    counter = Counter()
    rate = rospy.Rate(counter.frequency) # checks for a frequency param tag in the launchfile
    rospy.loginfo("Counter started!")

    rospy.logwarn("Saving to file: {}".format(counter.filename_ext))

    while not rospy.is_shutdown():
        rate.sleep()
        counter.nas_val()
        # arr = np.array(counter.nas)
        arr = np.array([counter.nas, counter.time])
        # rospy.loginfo(arr)
        np.save("{}.npy".format(counter.filename_ext), arr)

if __name__ == '__main__':
	main()
