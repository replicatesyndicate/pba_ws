#!/usr/bin/env python
from datetime import datetime
import rospy
from geometry_msgs.msg import Pose2D
import numpy as np

class Tracker(object):
    """
    Add subscribers to global pose data for each Kobot unit, and attach their callbacks to collect error terms for gradient tracking.
    Assumes that the cue area center is the origin of the world frame (i.e. center of cue area circle is at (0,0)).
    For other arena geometries, modify the callback function.
    """
    def __init__(self):
        self.get_params()
        self.datetime = datetime.now().strftime('%d_%m_%Y_%H_%M_%S')
        self.filename_ext = self.filename + '_' + self.datetime
        rospy.loginfo("Rate: {}, Topic: {}, Filename: {}".format(self.frequency,self.topic_name,self.filename_ext))
   
        self.pose = np.array([])

        self.subscriber = rospy.Subscriber(name= "/kobot0/{}".format(self.topic_name), 
                                            data_class= Pose2D,
                                            callback= self.pose2d_callback, 
                                            queue_size= 1)    
        
        self.data_received = False

    def get_params(self):
        """
        Get parameters for simulation or robot trials
        by default checks sim_params_aggregation.yaml or any identical parameter set
        /pose_tracker/floor, /pose_tracker/swarm, and /pose_tracker/node refer to sim_params_aggregation.yaml
        /pose_tracker_* parameters refer to individual private param tags for this script (add them to your launch file if they aren't provided by a yaml)
        """
        
        self.frequency = rospy.get_param('/pose_tracker/data_collection/frequency',default=120)

        if rospy.has_param('/pose_tracker/node'):
            self.topic_name = rospy.get_param('/pose_tracker/node/pose_topic')
        elif rospy.has_param('/pose_tracker/pose_tracker_pose_topic'):
            # alternative parameter naming for individual launching
            self.topic_name = rospy.get_param('/pose_tracker/pose_tracker_pose_topic')
        else:
            # not expecting the topic name to change too frequently
            self.topic_name = 'pose2d_global'

        if rospy.has_param('/pose_tracker/data_collection/filename'):
            self.filename = rospy.get_param('/pose_tracker/data_collection/filename')
            rospy.loginfo("Experiment name received. The file will be saved as {}.npy".format(self.filename))
        else:
            self.filename = "test"
            rospy.loginfo("No experiment name given. The file will be saved as test.npy")

        if (rospy.has_param('/pose_tracker/node') == True or rospy.has_param('/pose_tracker/pose_tracker_pose_topic') == True):
            pass
        else:
            rospy.logerr("Parameters not accessed. Check your launch file.")   
            raise

    def pose2d_callback(self,data):
        self.pose_data = data
        self.data_received = True
    
    def save_pose2d(self,data):
        if self.pose.size == 0:
            self.pose = np.array([data.x,data.y,data.theta])
        else:
            self.pose = np.vstack((self.pose,[data.x,data.y,data.theta]))

def main():
    rospy.init_node("pose_tracker", anonymous=False)
    tracker = Tracker()
    rate = rospy.Rate(tracker.frequency) # checks for a frequency param tag in the launchfile
    rospy.loginfo("Tracker started!")

    rospy.logwarn("Saving to file: {}".format(tracker.filename_ext))

    while not rospy.is_shutdown():
        rate.sleep()
        if tracker.data_received:
            tracker.save_pose2d(tracker.pose_data)
            # rospy.loginfo(arr)
            np.save("{}.npy".format(tracker.filename_ext), tracker.pose)

if __name__ == '__main__':
	main()
