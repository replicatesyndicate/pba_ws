import numpy as np
import matplotlib.pyplot as plt
import sys
import pandas as pd
# import matplotlib
# matplotlib.rcParams['font.family'] = ['serif']
# fig, axs = plt.subplots(1, 4,figsize=(15.5,8),sharex='col',sharey='row')

def main(filename):
	arr = np.load(filename)
	arr = arr[::26] # Vicon counts at 120 Hz, unnecessarily too fast - reduce to 5 Hz by counting once every 26 times.
	print(len(arr))
	print((sum(arr)/len(arr))) # average NAS value
	
	delarrindxend=np.arange(5400,19830) #take the first n seconds, delete the rest
	arr=np.delete(arr, delarrindxend)
	print(len(arr))
	# delarrindxend=np.arange(282000,330000)
	# arr=np.delete(arr, delarrindxend)
	# delarrindxend=np.arange(52000,72500)
	# arr=np.delete(arr, delarrindxend)
	# delarrindxend=np.arange(360000,380000)
	# arr=np.delete(arr, delarrindxend)
	# delarrindxend=np.arange(146630,169500)
	# arr=np.delete(arr, delarrindxend)
	# delarrindxend=np.arange(348600,348700)
	# arr=np.delete(arr, delarrindxend)
	# delarrindxend=np.arange(383000,700000)
	# arr=np.delete(arr, delarrindxend)

	# A="mean="+str(arr.mean())+str("\n")+"filename="+str(filename)
	# fig.suptitle(A, fontsize=16)
	# axs[0].set_xlabel("Data points", fontsize=16)
	# axs[0].set_ylabel("NAS", fontsize=16)
	# axs[1].set_xlabel("Data points", fontsize=16)
	# axs[1].set_ylabel("NAS", fontsize=16)
	# axs[2].set_xlabel("Data points", fontsize=16)
	# axs[2].set_ylabel("NAS", fontsize=16)
	# axs[3].set_xlabel("Data points", fontsize=16)
	# axs[3].set_ylabel("NAS", fontsize=16)
	# B="Windows:"+str(1)
	# C="Windows:"+str(3)
	# D="Windows:"+str(10000)
	# E="Windows:"+str(100000)
	# axs[0].title.set_text(B)
	# axs[1].title.set_text(C)
	# axs[2].title.set_text(D)
	# axs[3].title.set_text(E)
	# Q11,Q12,Q13=movingaverage(arr, 100)
	# Q21,Q22,Q23=movingaverage(arr, 1000)
	Q31,Q32,Q33=movingaverage(arr, 1)
	# Q41,Q42,Q43=movingaverage(arr, 3)
	# plotter(Q11,Q12,Q13,0)
	# plotter(Q21,Q22,Q23,1)	
	plotter(arr,Q32,Q33,0)	
	# plotter(Q41,Q42,Q43,2)
	# boxplotter(arr,1)	
	# boxplotter(arr,3)

	# np.save('corrected_forced_BCLST_3.npy',arr) # save the corrected numpy array as a file

	

def plotter(a1,a2,a3,i):
	# x=np.zeros(len(a2[0:199999]))
	# last=3*len(a2)/4
	# t=np.linspace(0,len(a1[0:last]),last)
	# print(len(t))
	# plt.plot(t,a2[0:last])
	plt.plot(a1)
	# locs, labels = plt.xticks()
	# # plt.xticks(np.arange(len(a2)))
	# # x=np.zeros(len(a2[0:199999]))
	# a3=a1[(last+1):len(a1)]
	# print (len(a3))
	# plt.boxplot(a3,positions=[last+last/30],notch=True,widths=last/20)
	# plt.xticks(locs)
	# x=np.arange(0,len(a2))
	# axs[i].fill_between(x,a2,a1,color='r',alpha=0.2)
	# axs[i].fill_between(x,a3,a2,color='b',alpha=0.2)

	plt.show()
# def boxplotter(a2,i):

	
def movingaverage(arr, window_size):
	# Convert array of integers to pandas series
	numbers_series = pd.Series(arr)
    
	# Get the window of series
	# of observations of specified window size
	windows = numbers_series.rolling(window_size)
    
	# Create a series of moving
	# averages of each window
	moving_averages = windows.mean()
    
	# Convert pandas series back to list
	moving_averages_list = moving_averages.tolist()
    
	# Remove null entries from the list
	avearr = moving_averages_list[window_size - 1:]
	avearr = np.array(avearr)

	# numbers_series = pd.Series(avearr)
	# windows = numbers_series.rolling(window_size)
	# av5= windows.quantile(.50)
	# av5 = av5.tolist()	
	# av5 = av5[window_size - 1:]
	# av5=np.array(av5)


	# numbers_series = pd.Series(avearr)
	# windows = numbers_series.rolling(window_size)
	# av7= windows.quantile(.75)
	# av7 = av7.tolist()	
	# av7 = av7[window_size - 1:]
	# av7=np.array(av7)

	# numbers_series = pd.Series(avearr)
	# windows = numbers_series.rolling(window_size)
	# av2= windows.quantile(.25)
	# av2 = av2.tolist()	
	# av2 = av2[window_size - 1:]
	# av2=np.array(av2)

	return(avearr,avearr,avearr)
	

if __name__ == '__main__':
	assert len(sys.argv) > 1, "No filename given"
	filename = sys.argv[1]
	main(filename)