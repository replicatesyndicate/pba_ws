import numpy as np
import matplotlib.pyplot as plt
import os
from time import time as TIME
from time import ctime
import matplotlib as mpl
mpl.rcParams['font.family'] = ['serif']
fig, ax = plt.subplots(1, 1,figsize=(15.5,8),sharex='col',sharey='row') 

#! path to folder containing .npy s to be plotted
input_folder_paths="~/pba_ws/src/kobot_behavior/aggregation/src/testing/model_comparison/"
output_path=input_folder_paths
pointN_percent=.02 # you will have pointN_percent*data point number of points in your graph
datasLen=602 # input number of points in NAS timeseries (?)
samplingPeriod=1 # for arranging x axis only

# palette=['r','r','b','b','g','g','purple','purple','orange','grey','pink','yellow','cyan',]
palette=['r','b','g','purple','orange','grey','pink','yellow','cyan',]
marker=["o","v","^","s","p","d"]

#! get complete path
allFiles=list(map(lambda x : os.path.join(input_folder_paths , x) , os.listdir(input_folder_paths)))
allFiles.sort()
arrays=[]
labels=[]

# remove files which are not npy type
for file in allFiles:
    #! only plot .npy files
    if os.path.splitext(file)[1]=='.npy':
        arrays.append( np.load(file) )
        labels.append(file)


#------------------------------------------------------------------------
def plotter(arr,label_,color):

    print(len(arr[1]))
    global pointN_percent
    pointN = int(pointN_percent * arr.shape[1])
    averagedData=np.zeros(( arr.shape[0] ,pointN))
    plt.sca(ax)
    window=np.shape(arr)[1]//pointN # 1000 is the number of points that i want to see in plot
    
    # averaging in time
    j=0
    for i in range( averagedData.shape[1] ):
        averagedData[:,i] = np.mean(arr[:,j:j+window],axis=1)
        j+=window
        
    # +1 is for injecting 0 in the beginning of array
    averagedDataMean=np.zeros((1,pointN+1))[0]
    averagedDataQ1=np.zeros((1,pointN+1))[0]
    averagedDataQ2=np.zeros((1,pointN+1))[0]
    # averaging and Q1 and Q3 for shades
    for i in range(pointN):
        averagedDataMean[1+i]=np.percentile(averagedData[:,i],50)
        averagedDataQ1[1+i]=np.percentile(averagedData[:,i],25)
        averagedDataQ2[1+i]=np.percentile(averagedData[:,i],75)

    x=np.arange(0,len(averagedDataMean))*((samplingPeriod*datasLen)/len(averagedDataMean))
    
    # last=int(len(x))
    # print(last)

    # locs, labels = plt.xticks()
    # plt.xticks(np.arange(last))
    # x=np.zeros(len(a2[0:199999]))
    # a3=averagedDataMean[(last+1):len(averagedDataMean)]
    # plt.boxplot(a3,positions=[107000],notch=True,widths=7000)
    # plt.xticks(locs)


    # plt.plot(x[0:last],averagedDataMean[0:last],color=color,label=label_) 
    # plt.fill_between(x[0:last],averagedDataMean[0:last],averagedDataQ2[0:last],color=color,alpha=0.2)
    # plt.fill_between(x[0:last],averagedDataQ1[0:last],averagedDataMean[0:last],color=color,alpha=0.2)

    if 'CRLA' in label_:
        plt.plot(x,averagedDataMean,color=color,label=label_,linewidth=4) 
    # elif 'RLA' in label_:
        # plt.plot(x,averagedDataMean, dashes=[4, 5],color=color,label=label_,linewidth=4)
    else:
        plt.plot(x,averagedDataMean,color=color,label=label_,linewidth=4)
    plt.fill_between(x,averagedDataMean,averagedDataQ2,color=color,alpha=0.2)
    plt.fill_between(x,averagedDataQ1,averagedDataMean,color=color,alpha=0.2)
    return averagedDataMean
#------------------------------------------------------------------------
for i in range( len(arrays) ):
    # label = os.path.basename(labels[i]).split('.')[0].split('x')[1]
    label = os.path.basename(labels[i]).split('.npy')[0].split('_')[1]
    x_axis = plotter(arrays[i],label,palette[i])
    print(label)
    
    # print(len(x_axis))
    # print("Overall mean:")
    # print(np.mean(x_axis))    
    print("Steady mean:")
    print(np.mean(x_axis[0:60]))
lines_labels = ax.get_legend_handles_labels()
fig.legend(lines_labels[0], lines_labels[1],ncol=2,loc='lower center',fontsize= 15.0,bbox_to_anchor=(0.5, 0.0))
# fig.legend(lines_labels[0], lines_labels[1],ncol=1,loc=1,fontsize=15.0,bbox_to_anchor=(1.0, 0.4))

# cheated by knowing the final time
divisionScale=1
FinalTime = 500000
xt=np.linspace(0,FinalTime,13)
# plt.xticks(xt,(xt//divisionScale).astype(int))
plt.yticks(fontsize=12)
plt.xticks(fontsize=12)
# plt.xlim(0,int(1e+6))

# plt.title('(a) Noiseless Setup $\mathbf{\sigma_n=0\degree}$',fontsize=15,fontweight='bold')
# plt.ylim(0,1)
# plt.xlabel('Time [s] / '+str(divisionScale//10),fontsize=15,fontweight='bold')
plt.xlabel('Time [s]',fontsize=20,fontweight='bold')
plt.ylabel('Normalized Aggregation Size',fontsize=20,fontweight='bold')
# plt.title("(Cue Area)/(Total Area): ~0.3 ",fontsize=20,fontweight='bold')
# plt.title("Cue radius: 0.3 [m], (Cue Area)/(Total Area): ~0.07 ",fontsize=20,fontweight='bold')
plt.title("Cue radius: 0.7 [m] ",fontsize=20,fontweight='bold')
# plt.title("Real Robot Experiments",fontsize=20,fontweight='bold')

plt.tight_layout()
plt.subplots_adjust(wspace=0.08,top=0.957,left=0.043,bottom=0.187,right=0.99,)

plt.savefig(os.path.join(output_path , 'model_comparison.png'))
plt.grid()
plt.show()
