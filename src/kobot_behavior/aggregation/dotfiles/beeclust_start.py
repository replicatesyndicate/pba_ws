#!/usr/bin/env python3
# Documentation: http://wiki.ros.org/roslaunch/API%20Usage#Roslaunch_file_with_command_line-style_arguments
import yaml
import roslaunch
import sys

uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
roslaunch.configure_logging(uuid)

pkg_name = 'aggregation'
launch_file = 'beeclust.launch'
# add in additional args here as a list of strings with ROS arg-style pairing e.g. 'ns:=kobot0'
additional_args = [] 

# sniff parameters from YAML to get namespace and robot count
dir = sys.path[0].rpartition('/')[0] + '/config/sim_params_{}.yaml'.format(pkg_name) # <behavior_name>/dotfiles/ -> <behavior_name>/config/sim_params_<behavior_name>.yaml
with open(dir, 'r') as file:
    params = yaml.safe_load(file)
drone_ns = params['node']['drone_namespace']
drone_num = params['swarm']['num_of_drone']

launch_files = []
for i in range(drone_num):
    sequence = [pkg_name, launch_file, 'ns:={}{}'.format(drone_ns,i)] + additional_args
    file = roslaunch.rlutil.resolve_launch_arguments(sequence)[0]
    args = sequence[2:]
    # append all roslaunch files and arguments
    launch_files.append((file,args)) # always in tuple form as namespace argument will always be provided

parent = roslaunch.parent.ROSLaunchParent(uuid, launch_files)
parent.start()
try:
    parent.spin()
except:
    parent.shutdown()
