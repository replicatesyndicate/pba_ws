This is a repository created to hold any Kobot related behavioral package. Packages created here are to be solely for behavioral applications to run on Kobot platforms. 

The user is to use the following in tandem with these applications and store any necessary additional programming in:
- `kobot_msgs` for all applications - generate messages
- `kobot_sw` for real ground robot applications - drivers
- `kinematic_sim` for simulations of any Kobot platforms - simulators and simulation drivers

# Package structure
This repository currently hosts package:
- aggregation

## Aggregation
Contains aggregation behavioral algorithms such as:
- Beeclust
- PBA
Any relevant behavior script should be set up in this package.

## Generic structure template
Follow these instructions and example packages to prepare your own behavior package.
`<alg>` denotes specific behavioral algorithm in a package.
`<pkg>` denotes the behavioral package itself.
### config
- `<alg>.yml`: store parameters for an algorithm itself. Is loaded for all execution cases, and by itself only for real robot applications.
- `sim_params_<pkg>.yaml`: store parameters to load in the kinematic simulator for this behavioral package. Read more about the structure and usage of these files in the `kinematic_sim` docpage.
### launch
`<alg>.launch`: master launch file for a real robot application. Will be loaded for all individual robots in simulations, by a simulation master launch file.
1. Generates the namespace for the robots, typically names like "kobot", or "telloz". Determines the naming scheme for robots launched for this algorithm.
2. Load parameters from `<pkg>/config/<app>.yml`
3. Run the node from `<pkg>/src/<app>.py` 
### src
Contains the node application, and any relevant scripts to run it.