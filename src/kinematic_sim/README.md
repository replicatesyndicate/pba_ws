# Kinematic Simulator
## Capabilities
* Environmental and robot initial conditions can be set using yaml config files. Robot initial conditions can also be randomized.
* Equations of motion use a 2D generic model, supporting movement along every direction.
* Robot actuation and sensor topics are contained in topics for separate topics. The user can publish their own twist commands. 
* Acceleration limits are now discarded. If required, they can be integrated with a motion model.
* Features faster obstacle collision and robot detection algorithms.
* The simulation speed can be tweaked to speed up or slow down experiments.
* OpenCV is the supported renderer. While using OpenCV, the user can also output debug data, such as robot and obstacle detections.
* If needed, all rendering can be disabled for batch simulations.

# Package structure
This repository contains the package `swarm_2d_sim`.

## swarm_2d_sim
This package contains the custom OpenCV-based simulator for Kobot units.

### dotfiles
This folder contains automated scripts to run multiple launchfiles for individual robots. Fundamentally the `.bash` and `.py` files serve the same purpose, and the Python scripts are currently the preferred option. The user will call these scripts through a master launch file, currently defined as `<pkg>_start.launch`.  
The required robot number information for namespace generation is created in `hosts.txt`. The user will provide the robot names in this file, which will also determine the robot **count** in simulations.

### launch
The master simulation launch files are located here. These launch files prepare the simulation environment, spawn multiple robots, then make them launch the desired behavioral package.  
The user is to set up a `<pkg>_start.launch` file, with its associated dotfile in `dotfiles`, and behavioral package in the repository `kobot_behavior` to launch their simulations.

### scripts
The simulation scripts are found here.
- `clock_provider.py`: provides discrete timestep information. Read more in this (docpage)[http://wiki.ros.org/Clock]
- `drone.py`: processes sensor and locomotion information to locate robots and simulate their interactions with their environment. Carries out the simulation for individual robots.
- `environment.py`: generates the arena in which the robots are going to be simulated in. Carries out rendering, drone sensing and physics(coordinates) for the simulation at large.
- `kobot_metrics.py`: carries out 2d pose calculations for the robots, using a specified method in the simulation configuration file given in `kobot_behavior/<pkg>/config/sim_params_<pkg>.yaml`.
- `renderer.py`: carries out the OpenCV image generation for the simulation.
- `sim_node.py`: starts the simulator from `simulator.py`. Runs by the node in the launchfiles in `swarm_2d_sim`.
- `sim_tests.ipynb`: contains some initial tests for the simulation implementation in a Jupyter notebook.
- `simulator.py`: simulates information regarding collisions, landmarks, floor sensors, and range-and-bearing sensors. Carries out the simulation at large. This is the main script with which the simulation is carried out.
- `utils.py`: handles module and class imports for the other scripts.

# How to use:
## Bring up the simulation
All the necessary components of the simulator has been concentrated on a single launch file. To launch the simulator for flocking or aggregation example simulations, you need to call:
```console
roslaunch flocking flocking_start.launch
```
... or,  

```console
roslaunch aggregation aggregation_start.launch
```

In the current version, the simulation is started after a topic is published such as below:
```console
rostopic pub /sim_clock_ready std_msgs/Empty "{}"
```

## Launch process for simulations

The hierarchy of the launch process is as the following:  
1. The user runs the `<pkg>_start.launch` file. (`<pkg>` for packages found in `kobot_behavior`)
2. The `/use_sim_time` param is set to True, to make the simulation use simulated time instead of real time. This is to be left as is for all simulations.
3. The node `clock_provider.py` from `swarm_2d_sim` will run to set up the simulation clock. Editing the `<param name ="freuquency" value="..."/>` line will generate a frequency parameter that determines the simulation stepsize `1 / clock_frequency`. The larger this value is, the smaller the discrete time steps get for the simulation. Simulation stability may depend on the tuning of this parameter. See `swarm_2d_sim/src/clock_provider.py` for more details.
4. The simulation node `sim_node.py` from `swarm_2d_sim` runs an instance of the Simulator class from `simulator.py`. The parameters to pass to the simulation itself are governed by the `sim_params_<pkg>.yaml` files found in `kobot_behavior` packages.
5. Finally, a node that prepares launchfiles for individual robots is executed:
```yaml
<node pkg="<pkg>" type="<pkg>_start.py" name="<pkg>_py" output="screen"/>
```
`<pkg>_start.py` calls `<pkg>.launch` multiple times for every robot defined in `<pkg>/dotfiles/hosts.txt`.

## Simulation parameters
See the relevant files in `kobot_behavior/<pkg>/config/sim_params_<pkg>.yaml`.  

#### width, height, pixel_per_mm
OpenCV window size in pixels. Arena size is (width * pixel_per_mm, height * pixel_per_mm) milimeters.

#### iteration_count
This parameter determines for how many simulation timesteps will the OpenCV window refreshes.

#### Other rendering parameters
These rendering parameters mostly self explanatory and they also have explanations on the yaml file. These basically control how much information should be rendered on the OpenCV window.

## Swarm parameters

#### num_of_drone
This is the simulated robot count. It must be same as the count from launch file.

#### randomize, initial_cond
If randomize is true, initial positions will be randomized. Otherwise, you need to provide `[x, y, theta]` for each robot in initial_cond parameter.

## Drone parameters
These parameters are self explanatory. They control max speeds, number of sensors and radius of the robot. The robot field-of-view (fov) and accuracy is also found here.

## Floor sensor parameters
The user can enable or disable the floor sensor in this section to mitigate performance drawbacks in simulations where floor sensors aren't required. 
Currently, a circular cue region is placed at `[center_x;center_y]` with the radii information provided by `xxx_radius_of_floor` parameters. The inner radius defines where the cue region begins to have maximal intensity, and the outer radius defines where the cue region extends towards. 
The `sensor_count` parameter and `radius_of_sensors` defines what kind of sensors will be simulated, and how will they be located on the robot. Note that the floor sensors are arranged in a circular array formation.

## Landmark parameters
The user can enable or disable landmark detection in this section to mitigate performance drawbacks in simulations where landmarks aren't required. 
`max_dist` and `fov` determine the cone of view of the landmarks for the robots: `max_dist` determining the distance, and `fov` determining the angular range at which the landmarks become visible.
Landmarks can also be placed by specifying their coordinates on the arena.

## Sensor parameters
This section defines the operating conditions of the simulated range-and-bearing sensors of the robots: the minimal and maximal viewing distances, angular resolution at which viewpoints are obtained, and the field-of-view for the range-and-bearing sensors.

## Metrics
The metrics for robot state calculations are selected here. The user can view their options in `swarm_2d_sim/scripts/kobot_metrics.py`.

## Obstacles
The number of obstacles to place on the arena, along with their positions are provided in this section. The obstacles are defined to be polygonal, and the user may provide any shape by adding its corner points in a list of `[x,y]` coordinates to the `vertices` list.

## Node parameters
These parameters set up the topic names to communicate sensor information and actuation commands between the simulator and the behavioral nodes. 
