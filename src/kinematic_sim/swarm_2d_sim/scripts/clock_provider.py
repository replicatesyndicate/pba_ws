#!/usr/bin/env python3

import time
import rospy
from std_msgs.msg import Time,Duration, Empty
from rosgraph_msgs.msg import Clock


class ClockProvider():
    def __init__(self):
            self.clock_pub = rospy.Publisher("/clock", Clock, queue_size = 10)
            self.clock = Clock()

            self.clock_sub = rospy.Subscriber("/sim_clock_ready", Empty, self.clock_callback)
            self.clock_pub.publish(self.clock)

            # Publish frequency
            self.duration = rospy.Duration.from_sec(1.0 / rospy.get_param('~frequency'))



    def clock_callback(self,msg):
        self.clock_pub.publish(self.clock)
        self.clock.clock = self.clock.clock + self.duration



if __name__ == '__main__':
    rospy.init_node('sim_clock', anonymous = True)
    clock_provider = ClockProvider()
    rospy.spin()
