#!/usr/bin/env python3

import cv2
from threading import Thread

class ImageRenderer():
    def __init__(self, frame=None) -> None:
        self.frame = frame
        self.stopped = False
        self.img_thread = None
        self.render = True

    def start_rendering(self):
        self.img_thread = Thread(group=None, target=self.show, args=(), daemon=True)
        self.img_thread.start()
        return self

    def show(self):   
        while not self.stopped and self.render:
            cv2.imshow("2D Simulation", self.frame)
            cv2.waitKey(10)            

    def stop(self):
        print('Stopping Rendering Thread')
        self.stopped = True
        self.img_thread.join()