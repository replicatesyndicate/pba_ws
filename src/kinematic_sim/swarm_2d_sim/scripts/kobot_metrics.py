#!/usr/bin/env python3
import numpy as np
from enum import Enum

class KobotMetric():
    robot_position_history = None
    robot_heading_history = None
    metric_history = None
    

    class MetricType(Enum):
        PositionOnly = 1,
        HeadingOnly = 2,
        PositionAndHeading = 3

    def __init__(self, name:str, robot_count: int, state_length:int, metric_type: MetricType):
        self.name = name
        self.robot_count = robot_count
        self.state_length = state_length
        self.metric_type = metric_type


    # Get Robot Positions from simulation and store if needed
    def getRobotPositions(self, positions):
        if self.metric_type == KobotMetric.MetricType.HeadingOnly:
            pass
        if self.robot_position_history is None:
            self.robot_position_history = np.array(positions)
        else:
            np.append(self.robot_position_history,positions)

    def getRobotHeadings(self, headings):
        if self.metric_type == KobotMetric.MetricType.PositionOnly:
            pass
        if self.robot_heading_history is None:
            self.robot_heading_history = np.array(headings)
        else:
            np.append(self.robot_heading_history, headings)


    def calculateMetric(self):
        pass

    def __str__(self) -> str:
        return str(self.name)



class AngularOrderMetric(KobotMetric):
    def __init__(self, robot_count, state_length):
        super().__init__("Angular Order Metric", robot_count, state_length, KobotMetric.MetricType.HeadingOnly)
        

    def calculateMetric(self):
        sum_x = 0
        sum_y = 0

        headings = self.robot_heading_history[::-1]
        for heading in headings:
            sum_x += np.cos(heading)
            sum_y += np.sin(heading)
        mag = np.linalg.norm([sum_x, sum_y])
    
        if len(headings) == 0:
            return 0
        return float(mag / len(headings))
