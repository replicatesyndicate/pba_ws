// Clock server for ROS Noetic
// Modified by:
// Atakan Botasun <atakanbotasun@outlook.com>
// 
// Original author:
// Juan Pablo Ramirez <pablo.ramirez@utdallas.edu>
// The University of Texas at Dallas
// Sensing, Robotics, Vision, Control and Estimation Lab
// (SeRViCE) 2012-2016
//

#include <ros/ros.h>
#include <rosgraph_msgs/Clock.h>
#include <std_msgs/Empty.h>

class clockserver
{
ros::Publisher clockpub;
ros::Time ctime;

double tfactor, realfactor;
bool clock_start = 0;

ros::WallTimer timer;

public:
ros::NodeHandle nh_;

clockserver()
{

	nh_.param("/clock_timefactor", tfactor, 1.0);
	if(tfactor < 0.0)
	tfactor = 1.0;
	clockpub = nh_.advertise<rosgraph_msgs::Clock>("/clock", 2);
	ROS_INFO_STREAM("Clock server created");

	nh_.param("/clock_frequency", realfactor, 250.0);
	ROS_INFO_STREAM("Time factor: " + std::to_string(tfactor) + " Frequency: " + std::to_string(realfactor));
	
	timer = nh_.createWallTimer(ros::WallDuration(1.0/realfactor), &clockserver::broadcastTime, this);

	ctime.sec = 0;
	ctime.nsec = 0;
}

~clockserver()
{
}

void clockStartCallback(const std_msgs::Empty::ConstPtr& message)
{
	clock_start = 1;
	ROS_INFO_STREAM("Starting clock!");
}

void broadcastTime(const ros::WallTimerEvent& te)
{
	if (clock_start){
		ROS_INFO_STREAM("Clock running!");
		ros::Duration d(tfactor/realfactor);
		ctime = ctime + d;

		rosgraph_msgs::Clock clockmsg;
		clockmsg.clock.sec = ctime.sec;
		clockmsg.clock.nsec = ctime.nsec;

		clockpub.publish(clockmsg);
	}
}

};

int main(int argc, char** argv)
{
ros::init(argc, argv, "clockserver");
clockserver dr;
ros::Subscriber clockStartSub = dr.nh_.subscribe("/sim_clock_ready", 1000, &clockserver::clockStartCallback, &dr);
ros::spin();
return 0;
}