#!/usr/bin/env python3
import numpy as np
import rospy
from geometry_msgs.msg import Twist, Pose2D, PolygonStamped, Point32, Quaternion
from std_msgs.msg import UInt8MultiArray, Float32, Empty
from nav_msgs.msg import Odometry
import tf #only needed for quaternion-euler angle transformations, any alternative is accepted (no broadcasts)
from shapely.geometry.point import Point
from shapely.geometry import Polygon
from drone import Drone
from environment import Environment
import math

from kobot_msgs.msg import range_n_bearing_sensor, range_n_robot_detector, landmark_pose, floor_sensor
from renderer import ImageRenderer
from rosgraph_msgs.msg import Clock
from kobot_metrics import AngularOrderMetric
import utils

class Simulator():
    def __init__(self) -> None:
        self.landmark_parameters = []
        self.drone_parameters = []
        self.floor_parameters = []
        self.sensor_parameters = []
        self.sim_parameters = []
        self.swarm_parameters = []
        self.node_parameters = []
        self.obstacle_parameters = []
        
        self.range_pubs = []
        self.floor_pubs = []
        self.heading_pubs = []
        self.landmark_pubs = []
        self.pose_pubs = []
        self.odom_pubs = []
        self.ics_pubs = [] # need to publish initial conditions directly to avoid lag related TF setup issues

        #Pseudo-odometry preparatory information
        self.twist_vals = [] #sizing after num_of_drone known
        self.ics = []

        self.iteration_count = 0

        self.metrics = []
        self.metric_pubs = []

        # Get All Parameters from the server
        if rospy.has_param('/use_sim_time'):
            rospy.wait_for_message('/clock', Clock)
            rospy.loginfo('Received Simulation Clock')
        
        if rospy.has_param('~floor'):
            rospy.loginfo('Floor Parameters Received')
            self.floor_parameters = rospy.get_param('~floor')
            rospy.loginfo(str(self.floor_parameters))
        else:
            rospy.logfatal('Could not receive floor parameters.')
            rospy.signal_shutdown('Failed Initialization')

        if rospy.has_param('~sim'):
            rospy.loginfo('Simulation Parameters Received')
            self.sim_parameters = rospy.get_param('~sim')
            rospy.loginfo(str(self.sim_parameters))
        else:
            rospy.logfatal('Could not receive sim params')
            rospy.signal_shutdown('Failed Initialization')

        if rospy.has_param('~swarm'):
            rospy.loginfo('Swarm Parameters Received')
            self.swarm_parameters = rospy.get_param('~swarm')
            rospy.loginfo(str(self.swarm_parameters))
        else:
            rospy.logfatal('Could not receive Swarm params')
            rospy.signal_shutdown('Failed Initialization')

        if rospy.has_param('~drone'):
            rospy.loginfo('Drone Parameters Received')
            self.drone_parameters = rospy.get_param('~drone')
            rospy.loginfo(str(self.drone_parameters))
        else:
            rospy.logfatal('Could not receive Drone params')
            rospy.signal_shutdown('Failed Initialization')

        if rospy.has_param('~sensor'):
            rospy.loginfo('Sensor Parameters Received')
            self.sensor_parameters = rospy.get_param('~sensor')
            rospy.loginfo(str(self.sensor_parameters))
        else:
            rospy.logfatal('Could not receive Sensor params')
            rospy.signal_shutdown('Failed Initialization')

        if rospy.has_param('~node'):
            rospy.loginfo('Node Parameters Received')
            self.node_parameters = rospy.get_param('~node')
            rospy.loginfo(str(self.sensor_parameters))
        else:
            rospy.logfatal('Could not receive Node params')
            rospy.signal_shutdown('Failed Initialization')

        if rospy.has_param('~obstacles'):
            rospy.loginfo('Obstacle Parameters Received')
            self.obstacle_parameters = rospy.get_param('~obstacles')
            rospy.loginfo(str(self.obstacle_parameters))
        else:
            rospy.logfatal('Could not receive Obstacle params')
            rospy.signal_shutdown('Failed Initialization')

        if rospy.has_param('~metrics'):
            rospy.loginfo('Metrics Parameters Received')
            self.metrics_parameters = rospy.get_param('~metrics')
            rospy.loginfo(str(self.metrics_parameters))
        else:
            rospy.logfatal('Could not receive Metrics params')
            rospy.signal_shutdown('Failed Initialization')

        if rospy.has_param('~landmark'):
            rospy.loginfo('Landmark Parameters Recieved')
            self.landmark_parameters = rospy.get_param('~landmark')
            rospy.loginfo(str(self.landmark_parameters))
        else:
            rospy.logfatal("Could not receive Landmark params")
            rospy.signal_shutdown('Failed Initialization')


        #Landmark preparatory information
        self.landmark_default_ori = self.landmark_parameters["default_aruco_orientation"]
        self.landmark_ori_list = self.landmark_parameters["landmark_orientations"]

        obstacles = []
        for i in range(self.obstacle_parameters['num_of_obstacles']):
            pt_array = self.obstacle_parameters['vertices'][i]
            points = []

            for j in range(len(pt_array)):
                p = Point(np.array(pt_array[j]))
                points.append(p)

            obstacles.append(points)

        drone_list = []
        self.ics = [[0,0,0]] * self.swarm_parameters["num_of_drone"]
        if self.swarm_parameters['randomize']:
            # ic = list(10*np.random.rand(self.swarm_parameters['num_of_drone'],3))
            for i in range(self.swarm_parameters["num_of_drone"]):
                point_found = 0

                while not point_found:
                    # ic = 0.1*np.random.uniform(3) + np.array([1,2,0])
                    width = self.sim_parameters["width"] / (self.sim_parameters["pixel_p_mm"]*1000) #in meters
                    height = self.sim_parameters["height"] / (self.sim_parameters["pixel_p_mm"]*1000)
                    x = np.random.uniform(-width/2, width/2)
                    y = np.random.uniform(-height/2, height/2)
                    theta = 0
                    ic = np.array([x, y, theta])
                    self.ics[i] = ic
                    ic_point = Point(np.array([x, y]))
                    collision = 0

                    for obstacle in obstacles:
                        if ic_point.within(Polygon(obstacle)):
                            collision = 1
                            break

                    if not collision:
                        drone = Drone(self.landmark_parameters, self.floor_parameters ,self.drone_parameters, self.sensor_parameters, 
                                ic, i, self.sim_parameters['drone_radius'])
                        
                        for temp_drone in drone_list:
                            if drone.boundary.intersects(temp_drone.boundary):
                                collision = 1
                                break

                        if not collision:
                            drone_list.append(drone)
                            point_found = 1

        else:
            for i in range(self.swarm_parameters["num_of_drone"]):
                drone_list.append(Drone(self.landmark_parameters, self.floor_parameters, self.drone_parameters, self.sensor_parameters, 
                    np.array(self.swarm_parameters['initial_cond'][i]), i, self.sim_parameters['drone_radius']))
            self.ics = self.swarm_parameters['initial_cond'] # required for odometry

        self.env = Environment(self.sim_parameters, self.floor_parameters, self.landmark_parameters, self.sensor_parameters, drone_list)   
        for obstacle in obstacles:
            self.env.add_obstacle_vertex_list(obstacle) 


        for i in range(len(self.metrics_parameters["metric_names"])):
            self.metrics.append(utils.str_to_class("kobot_metrics", self.metrics_parameters["metric_names"][i])(self.swarm_parameters["num_of_drone"], 3))
            self.metric_pubs.append(rospy.Publisher("/" + self.metrics_parameters["topic_names"][i], Float32, queue_size= 10))
        

        # self.update_rate = rospy.Rate(1000.0)
        # self.update_rate = rospy.Rate(math.ceil(1.0 / self.sim_parameters['delta_time']))
        self.iteration_count = self.sim_parameters["iteration_count"]
        self.iteration = 0

        if self.sim_parameters['render_frame']:
            self.renderer = ImageRenderer()
            self.renderer.frame = np.zeros((self.sim_parameters['width'], self.sim_parameters['height']), np.int8)
            self.renderer.start_rendering()

        if self.sim_parameters['publish_obstacles']:
            self.obs_publishers = []
            for i in range(self.obstacle_parameters['num_of_obstacles']):
                self.obs_publishers.append(rospy.Publisher('/' + self.sim_parameters['obstacle_topic'] + str(i), PolygonStamped, queue_size=10))
            
            self.obs_msg = PolygonStamped()
            self.obs_msg.header.frame_id = self.sim_parameters['tf_parent_frame']
            self.obs_timer = rospy.Timer(rospy.Duration(self.sim_parameters['obstacle_publish_period']), self.obstacle_publish_callback)

        self.twist_vals = [(0,0,0)] * self.swarm_parameters["num_of_drone"] #sizing twist_vals
        for i in range(self.swarm_parameters["num_of_drone"]):
            rospy.Subscriber('/' + self.node_parameters['drone_namespace'] + str(i) + '/' + self.node_parameters['twist_topic'], Twist, self.twist_command_callback, i)
            self.range_pubs.append(rospy.Publisher('/' + self.node_parameters['drone_namespace'] + str(i) + '/' + self.node_parameters['range_scan_topic'], range_n_bearing_sensor, queue_size=100))
            self.heading_pubs.append(rospy.Publisher('/' + self.node_parameters['drone_namespace'] + str(i) + '/' + self.node_parameters['heading_topic'], Float32, queue_size=100))
            self.pose_pubs.append(rospy.Publisher('/' + self.node_parameters['drone_namespace'] + str(i) + '/' + self.node_parameters['pose_topic'], Pose2D, queue_size=100))
            self.odom_pubs.append(rospy.Publisher('/' + self.node_parameters['drone_namespace'] + str(i) + '/' + self.node_parameters['odom_topic'], Odometry, queue_size=100))
            self.ics_pubs.append(rospy.Publisher('/' + self.node_parameters['drone_namespace'] + str(i) + '/' + "ics", Pose2D, queue_size=100))

            if self.floor_parameters["enabled"]:
                self.floor_pubs.append(rospy.Publisher('/'+ self.node_parameters['drone_namespace'] + str(i)+ '/' + self.node_parameters['floor_sensor_topic'], floor_sensor, queue_size = 100))
            if self.landmark_parameters["enabled"]:
                # self.landmark_pubs.append(rospy.Publisher('/'+ self.node_parameters['drone_namespace']+str(i)+ '/' + self.node_parameters['landmark_sensor_topic'], landmark, queue_size=100))
                self.landmark_pubs.append(rospy.Publisher('/'+ self.node_parameters['drone_namespace']+str(i)+ '/' + self.node_parameters['landmark_sensor_topic'], landmark_pose, queue_size=100))

        self.prev_time = rospy.Time.now()
        self.clock_ready_publisher = rospy.Publisher("/sim_clock_ready", Empty, queue_size=100)
        self.clock_ready_publisher.publish()
        
    
    def sim_loop(self):
        # We will render if this is true
        # self.clock_msg = rospy.Time.from_sec(float(self.iteration) * self.sim_parameters['delta_time'])
        # self.clock_publisher.publish(self.clock_msg)
        delta_time = (rospy.Time.now().to_sec()-self.prev_time.to_sec())
        self.prev_time = rospy.Time.now()
        if self.iteration == self.iteration_count:
            
            if self.sim_parameters['render_frame']:

                # print(rospy.Time.now().to_sec())
                # print(delta_time)
                rendered_img, data, robot_detect_data = self.env.render(delta_time)
            
                self.renderer.frame = rendered_img
            else:
                data, robot_detect_data = self.env.get_drone_data(delta_time)
            
            self.iteration = 0
            

            laser_msg = range_n_bearing_sensor()
            heading_msg = Float32()
            #landmark_msg = landmark()
            landmark_msg = landmark_pose()

            for i in range(self.swarm_parameters["num_of_drone"]):
                # Publish Heading Data for every drone as radians
                heading_msg.data = self.env.drones[i].pose[2]
                self.heading_pubs[i].publish(heading_msg)
                pose_msg = Pose2D()
                odom_msg = Odometry()

                pose_msg.x, pose_msg.y, pose_msg.theta = self.env.drones[i].pose[0], self.env.drones[i].pose[1], self.env.drones[i].pose[2] 
                self.pose_pubs[i].publish(pose_msg)
                
                pos_x = self.env.drones[i].pose[0] - self.ics[i][0]
                pos_y = self.env.drones[i].pose[1] - self.ics[i][1]
                ang_z = self.env.drones[i].pose[2] - self.ics[i][2]

                # transform to map frame (i.e. use heading IC and basic trigonometry to convert positional offsets into map coordinates)
                # this is simply a 2D coordinate transformation matrix [[x'],[y']]:= [[cQ -sQ], [sQ cQ]][[x],[y]]
                odom_x = pos_x * np.cos(-self.ics[i][2]) - pos_y * np.sin(-self.ics[i][2])
                odom_y = pos_x * np.sin(-self.ics[i][2]) + pos_y * np.cos(-self.ics[i][2])

                #stamp sequence is equal to the iteration count
                #odom_msg.header.seq = self.iteration #this should be automatically set
                odom_msg.header.stamp = rospy.Time.now()
                odom_msg.header.frame_id = 'odom_' + str(self.node_parameters['drone_namespace']) + str(i)
                odom_msg.child_frame_id = 'base_link_' +  str(self.node_parameters['drone_namespace']) + str(i)
                #Pose information generated in the robot's own local frame (init at [0,0,0] + forward and lateral changes per tick)
                odom_msg.pose.pose.position.x = odom_x
                odom_msg.pose.pose.position.y = odom_y
                odom_msg.pose.pose.position.z = 0
                #NOTE: this is the only TF call in this code. If desired, an equivalent method can replace the following statement
                q = tf.transformations.quaternion_from_euler(0,0,ang_z)
                odom_msg.pose.pose.orientation = Quaternion(*q)
                #NOTE: currently assuming infinite acceleration - therefore twist msg is equiv. to current twist vector
                #if Drone.update_speed() is modified to be otherwise, fix the following statement
                #[twist_msg.linear.x,twist_msg.linear.y,twist_msg.angular.z]
                tv = self.twist_vals[i]
                odom_msg.twist.twist.linear.x = tv[0]
                odom_msg.twist.twist.linear.y = tv[1]
                odom_msg.twist.twist.linear.z = 0
                odom_msg.twist.twist.angular.x = 0
                odom_msg.twist.twist.angular.y = 0
                odom_msg.twist.twist.angular.z = tv[2]
                #NOTE: see broadcast_tf.py in kobot_behavior if a TF call for this message is required
                self.odom_pubs[i].publish(odom_msg)       
                #NOTE: using this initial condition publisher as the initial TF broadcaster step suffers from lag (initial conditions cannot be safely determined)
                
                self.ics_pubs[i].publish(Pose2D(x=self.ics[i][0],y=self.ics[i][1],theta=self.ics[i][2]))

        
                #Publish floor sensor data for every drone as 8 bit integer 0-255
                if self.floor_parameters["enabled"]:
                    # Publish floor sensor data for every drone as 8 bit integer 0-255
                    floor_sensor_msg = floor_sensor()
                    floor_sensor_msg.intensity = self.env.drones[i].floor_sensor_result
                    self.floor_pubs[i].publish(floor_sensor_msg)

                if self.landmark_parameters["enabled"]:
                    # Publish landmark sensor data.
                    landmark_output = self.env.drones[i].landmark_sensor_result

                    #Currently resolving in Pose2D conventions and information wrt the global frame. If using 3D pose information, extend the angles.
                    landmark_msg = landmark_pose()
                    
                    
                    if landmark_output[0]: #publish only in vicinity of landmarks
                        landmark_msg.id = landmark_output[1].astype(np.uint8)
                        # get euclidean distance as translation in <cam_link/z>, no displacement in <cam_link/x,y> as the vector is center-to-center
                        # in real robot cases, this is not exactly accurate, but the ideal case should be <cam_link/0,0,d>.
                        landmark_msg.tvec = np.asarray([0.0, 0.0, landmark_output[3]])
                        # default landmark orientation refers to a landmark in the case
                        # where the angle between its frontal axis and the robot's vision axis is 180 degrees
                        # i.e. the robot's *camera* is looking straight at the landmark
                        # landmark_output[2] is the view angle between the camera and the landmark
                        
                        landmark_msg.rvec = np.asarray([
                            np.deg2rad(self.landmark_default_ori[0]),
                            np.deg2rad(self.landmark_default_ori[1]) + landmark_output[2],
                            np.deg2rad(self.landmark_default_ori[2])
                        ])
                        
                        # # use the debug below only when the landmark_output length is 5 (typically 4)
                        # rospy.logwarn("[simulator.py]: view angle (used vs offset): {}, {}".format(landmark_output[2],landmark_output[4]))
                        # rospy.logwarn("[simulator.py]: tvec: {}".format(landmark_msg.tvec,type(landmark_msg.tvec)))
                        self.landmark_pubs[i].publish(landmark_msg)

            
                # Collect Range Scan Measurements
                laser_msg.range_n_bearing.clear()
                for j in range(self.drone_parameters["num_of_sensor"]):
                    robot_msg = range_n_robot_detector()
                    robot_msg.is_robot = robot_detect_data[i][j]
                    robot_msg.range = int(data[i][j])
                    laser_msg.range_n_bearing.append(robot_msg)
                self.range_pubs[i].publish(laser_msg)

        else:
            self.env.physics_update(delta_time)
            self.iteration += 1

        # We can stack states of the robots before the iteration above and we can use it here too.
        # This can be a speedup
        headings = []
        positions = []

        for i in range(self.swarm_parameters["num_of_drone"]):
            headings.append(self.env.drones[i].pose[2])
            positions.append(self.env.drones[i].pose[0:2])

        headings = np.array(headings)
        positions = np.array(positions)

        for i in range(len(self.metrics)):
            metric = self.metrics[i]
            metric.getRobotPositions(positions)
            metric.getRobotHeadings(headings)
            metric_message = Float32()
            
            metric_message.data = metric.calculateMetric()
            self.metric_pubs[i].publish(metric_message)

        self.clock_ready_publisher.publish()
        

    def on_shutdown(self):
        print('Killing the simulator Node')
        self.renderer.stop()
        

    def twist_command_callback(self, twist_msg: Twist, drone_id):
        self.env.get_drone_by_id(drone_id).update_speed(np.array([twist_msg.linear.x, twist_msg.linear.y, twist_msg.angular.z]))
        #added this as an attribute to pass twist information
        #remove if speed information is obtained from other sources, i.e non-inf acceleration
        self.twist_vals[drone_id] = (twist_msg.linear.x,twist_msg.linear.y,twist_msg.angular.z)

    
    def obstacle_publish_callback(self, timer):
        
        for i in range(self.obstacle_parameters['num_of_obstacles']):
            pt_array = self.obstacle_parameters['vertices'][i]
            points = []
            
            
            for j in range(len(pt_array)):
                temp = np.array(pt_array[j])
                p = Point32(temp[0] / 1000.0, temp[1] / 1000.0, 0) # Convert to m
                points.append(p)

            self.obs_msg.polygon.points = points
            self.obs_msg.header.stamp = rospy.Time.now()
            self.obs_publishers[i].publish(self.obs_msg)