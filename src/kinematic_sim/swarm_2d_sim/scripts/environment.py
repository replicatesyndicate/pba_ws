#!/usr/bin/env python3
from shapely.geometry import Polygon, LineString, Point
import cv2
import math
import numpy as np
from drone import Drone


class Environment():
    def __init__(self, parameters, floor_parameters, landmark_parameters, sensor_parameters, drones) -> None:
        self.parameters = parameters
        self.floor_parameters = floor_parameters
        self.landmark_parameters = landmark_parameters
        self.drones = drones
        self.sensor_parameters = sensor_parameters

        self.max_w = self.parameters["width"] / self.parameters["pixel_p_mm"]//2
        self.max_h = self.parameters["height"] / self.parameters["pixel_p_mm"]//2
        
        self.obstacles = []
        self.intersection_list = []
        
        self.time = 0
        self.frame = np.zeros((self.parameters["height"], self.parameters["width"], 3), np.uint8)
        self.origin = self.parameters["width"]//2, self.parameters["height"]//2
        h,w = float(self.parameters["height"])  / (2.0 * self.parameters["pixel_p_mm"]), float(self.parameters["width"]) / (2.0 * self.parameters["pixel_p_mm"])


        # Add four obstacles for the boundary since shapely cant
        # find correct distance when an object is inside boundary
        # this is for the outer boundary not needed for inner obstacles
        eps = 0.001
        self.add_obstacle([-w,-h], [-w - eps, h], [-w - eps,h], [-w, h])
        self.add_obstacle([w, -h], [w, h], [w + eps,h], [w + eps, -h])
        self.add_obstacle([w, -h], [w, -h - eps], [-w, -h - eps], [ -w, -h])
        self.add_obstacle([-w, h], [-w, h + eps], [w, h + eps], [w, h])
        

    def render_intersections(self, frame):
        intersects = np.array(self.intersection_list)

        for i in range(intersects.shape[0]):
            x,y = intersects[i,0,0], intersects[i,0,1]
            x_next, y_next = intersects[i, 1, 0], intersects[i, 1, 1]
            cv2.line(frame, (int(x * self.parameters["pixel_p_mm"]) + self.origin[0], -int(y * self.parameters["pixel_p_mm"]) + self.origin[1]), (int(x_next) +  self.origin[0], -int(y_next) + self.origin[1]), (255, 0, 0), 1, cv2.LINE_AA)
            cv2.circle(frame, (math.ceil(x_next) +  self.origin[0], -math.ceil(y_next) + self.origin[1]), 3, (147, 20, 255))
 
    def init_render_frame(self):
        """
        Use this function to (re)initialize the frame rendered
        with obstacles, floor, and landmark.
        """
        self.frame = np.zeros((self.parameters["height"], self.parameters["width"], 3), np.uint8)
        for obs in self.obstacles:
            obsc = np.array(np.array(list(obs.exterior.coords), np.float64) * self.parameters["pixel_p_mm"], np.int64)
            obsc[:,1] = -obsc[:,1]
            obsc = obsc + self.origin
            cv2.drawContours(self.frame, [obsc], -1, (0, 0, 255), 5)


        if self.floor_parameters["enabled"]:
            center_of_floor_sensor = np.multiply(np.array([self.floor_parameters["center_x"], self.floor_parameters["center_y"]]), self.parameters["pixel_p_mm"]) + self.origin
            center_of_floor_sensor = center_of_floor_sensor.astype(int)
            
            inner_radius = int(self.floor_parameters["inner_radius_of_floor"]*self.parameters["pixel_p_mm"])
            outer_radius = int(self.floor_parameters["outer_radius_of_floor"]*self.parameters["pixel_p_mm"])
            
            for i in range(255):
                grayscale_value = i
                self.frame = cv2.circle(self.frame, center_of_floor_sensor, outer_radius - int((outer_radius-inner_radius) * i / 255), (grayscale_value, grayscale_value, grayscale_value), -1)
        
        if self.landmark_parameters["enabled"]:
            for landmark in self.landmark_parameters["landmark_coordinates"]:
                coordinates_of_current_landmark = np.multiply(landmark,self.parameters["pixel_p_mm"]) + self.origin
                coordinates_of_current_landmark = coordinates_of_current_landmark.astype(int)
                self.frame = cv2.circle(self.frame, coordinates_of_current_landmark, 7, (0,255,255), -1)

    def add_obstacle(self, pt1, pt2, pt3, pt4):
        self.obstacles.append(Polygon([pt1, pt2, pt3, pt4]))

    # Receives either Point object array or np.array structure
    def add_obstacle_vertex_list(self, pts):
        self.obstacles.append(Polygon(pts))

    def physics_update(self,delta_time):
        self.time += delta_time
        for drone in self.drones:
            drone.update_coords(delta_time)

    def floor_sensor_update(self):
        for drone in self.drones:
            drone.update_floor_sensor()

    def landmark_sensor_update(self):
        for drone in self.drones:
            drone.update_landmark_sensor()

    def get_drone_data(self, delta_time):
        self.physics_update(delta_time)
        self.floor_sensor_update()
        self.landmark_sensor_update()
        range_all_robots = []
        is_robot_all_robots = []
        self.intersection_list.clear()
        
        for drone in self.drones: # will check each drone
            range_arr, is_robot_arr = drone.range_n_bearing(self.drones, self.obstacles)
            is_robot_all_robots.append(is_robot_arr)
            range_all_robots.append(range_arr)
            sensor_angles = np.arange(0, np.pi*2, np.pi*2/drone.parameters["num_of_sensor"])
            for i, sensor_direction in enumerate(sensor_angles): # will check each sensor
                sensor_direction =  drone.pose[2] + sensor_direction # sensor_direction in fixed frame
                range = range_arr[i]
                is_robot = is_robot_arr[i]

                # Convert to mm
                x = drone.pose[0] * 1000.0
                y = drone.pose[1] * 1000.0

                # full line segment of from sensor to object its ray hit
                x_end = (x + (np.cos(sensor_direction) * range)) * self.parameters["pixel_p_mm"]
                y_end = (y + (np.sin(sensor_direction) * range)) * self.parameters["pixel_p_mm"]

                if self.parameters['draw_all_laser']:
                    self.intersection_list.append([[x, y], [x_end, y_end]])
                elif self.parameters['draw_obs_intersect'] and range < self.sensor_parameters['max_dist'] and not is_robot:
                    self.intersection_list.append([[x, y], [x_end, y_end]])
                elif self.parameters['draw_robot_detection'] and is_robot:
                    self.intersection_list.append([[x, y], [x_end, y_end]])


        return np.array(range_all_robots), np.array(is_robot_all_robots)


    def render(self,delta_time):
        range_data, robot_detect_data = self.get_drone_data(delta_time)
        temp_frame = []
        if self.parameters['render_frame']:
            temp_frame = np.copy(self.frame)

            for drone in self.drones:
                temp = np.array(drone.pose[0:2] * 1000.0 * self.parameters["pixel_p_mm"], np.int32)
                temp[1] = -temp[1]
                pt = np.array((temp) + self.origin, np.int64)
                
                if self.parameters['draw_arrow']:
                    theta = drone.pose[2]
                    temp_end = np.multiply(self.parameters['arrow_length'], np.array([np.cos(theta), -np.sin(theta)]))
                    end_pt = np.array(temp + temp_end + self.origin, np.int64)

                    cv2.arrowedLine(temp_frame, pt, end_pt, 
                        (0, 255, 0), int(self.parameters['arrow_thickness']))
                    
                cv2.circle(temp_frame, pt, int(self.parameters['drone_radius'] * self.parameters["pixel_p_mm"] + 1.0), (255, 255, 100), -1)

            cv2.putText(temp_frame, "Time: " + str(round(self.time, 3)), (10, 30), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 67), 1)

        
            self.render_intersections(temp_frame)
        
        return temp_frame, range_data, robot_detect_data

    def get_drone_by_id(self, id) -> Drone:
        return self.drones[id]
