#!/usr/bin/env python3
import numpy as np
from shapely.geometry import LineString, Point, Polygon
from shapely.ops import nearest_points
import time 
import math

class Drone():
    def __init__(self, parameters_landmark, parameters_floor, parameters_drone, parameters_sensor, initial_pose = np.zeros((3)), id = -1, radius = 0.05) -> None:
        self.landmark_parameters = parameters_landmark
        self.floor_parameters = parameters_floor
        self.parameters = parameters_drone
        self.sensor_parameters = parameters_sensor
        
        self.id = int(id)
        self.radius = radius

        self.num_edges = 4
        ang_arr = np.arange(0,2*np.pi,2*np.pi/self.num_edges)
        self.x_coords = radius*np.cos(ang_arr)
        self.y_coords = radius*np.sin(ang_arr)

        # 2-D state is [X Y Theta]
        self.pose = initial_pose
        self.floor_sensor_result = [0]
        # 2-D vel is [Xdot Ydot ThetaDot]

        self.vel = np.zeros(3)
        self.num_sensors = self.parameters["num_of_sensor"]
        self.fov = self.parameters["fov"]*np.pi/180.0

        self.sensor_angles = np.arange(0, np.pi*2, np.pi*2/self.num_sensors)

        
        temp_x, temp_y = self.pose[0]*1000.0, self.pose[1]*1000.0 # mm
        xy_tuple = [tuple([temp_x+self.x_coords[i],temp_y+self.y_coords[i]]) for i in range(self.num_edges)]
        self.boundary = Polygon(xy_tuple)

 
    def update_speed(self, speed):
        """
        For now assume infinite acc. If an acceleration limit is required, fit
        a integrator with saturation later. Velocity is in body coordinates
        x is forward wrt to drone and y is to the left.   
        """
        self.vel = speed
    
    def update_coords(self, delta_time):
        c, s = np.cos(float(self.pose[2])), np.sin(float(self.pose[2]))
        rot_mat = np.array([[c, -s, 0.0], [s, c, 0.0], [0.0, 0.0, 1.0]])
        vel_global = rot_mat@self.vel
        self.pose = np.add(self.pose, delta_time*vel_global)
        self.pose[2] = wrap2pi(self.pose[2])

    def update_floor_sensor(self):
        if self.floor_parameters["enabled"]:
            self.floor_sensor_result = self.floor_sensor_calculator()

    def robot_detection(self,min_dist_arr,drones):
        # default vals
        # min_dist_arr = np.ones((self.num_sensors))*self.sensor_parameters["max_dist"]
        is_robot_arr = np.zeros((self.num_sensors),dtype=bool)
        for temp_drone in drones: # go over drones one by one
            if temp_drone.id == self.id:
                continue # drone should not see itself
            # distance to neighbor
            neighbor_pose = np.array([temp_drone.pose[0], temp_drone.pose[1]])
            dist = np.linalg.norm(self.pose[0:2]- neighbor_pose) * 1000.0 # mm
            if dist<self.sensor_parameters["max_dist"]: # a valid distance
                # compute which sensor should see the neighbor
                ang = np.arctan2(neighbor_pose[1]-self.pose[1],neighbor_pose[0]-self.pose[0])
                ang = -self.pose[2]+ang
                sensor_indx, misalignment = sensor_mapping(ang)
                if misalignment < self.fov: # is it inside the FoV
                    if dist < min_dist_arr[sensor_indx]: # is it closer than a prev. object
                        is_robot_arr[sensor_indx] = True
                        min_dist_arr[sensor_indx] = dist
        return min_dist_arr, is_robot_arr

    def obstacle_detection(self, obstacles):
        min_dist = self.sensor_parameters["max_dist"]
        min_dist_arr = np.ones((self.num_sensors))*min_dist
        robot_center = Point(self.pose[0:2]*1000.0)
        for poli in obstacles:
            npts = [o.coords[0] for o in nearest_points(robot_center,poli)]
            npts = npts[1]
            # print(self.pose[0:2]*1000.0 - np.array(npts))
            dist = np.linalg.norm(self.pose[0:2]*1000.0 - np.array(npts))
            # print(dist)
            if dist<self.sensor_parameters["max_dist"]: # a valid distance
                # compute which sensor should see the neighbor
                ang = np.arctan2(npts[1]-self.pose[1]*1000.0,npts[0]-self.pose[0]*1000.0)
                ang = -self.pose[2]+ang
                sensor_indx, misalignment = sensor_mapping(ang)
                if misalignment < self.fov: # is it inside the FoV
                    if dist < min_dist_arr[sensor_indx]: # is it closer than a prev. object
                        min_dist_arr[sensor_indx] = dist
        
        return min_dist_arr

    def range_n_bearing(self, drones, obstacles):
        range_arr = []
        is_robot_arr = []

        range_arr = self.obstacle_detection(obstacles)
        range_arr, is_robot_arr = self.robot_detection(range_arr, drones)
        return range_arr, is_robot_arr
    
    def floor_sensor_calculator(self):
        sensor_count = self.floor_parameters["sensor_count"]
        sensor_output = []
        alpha = 2 * math.pi / sensor_count
        center_coordinates = np.multiply(self.pose[0:2],1000)
        radius_of_sensors = self.floor_parameters["radius_of_sensors"]
        for i in range(1,sensor_count+1):
            sensor_coordinates_relative_to_center = np.multiply(np.array([np.cos(i*alpha), np.sin(i*alpha)]), radius_of_sensors)
            sensor_coordinates_real = np.add(sensor_coordinates_relative_to_center, center_coordinates)
            sensor_output.append(self.floor_sensor_calculator_from_coordinates(sensor_coordinates_real))  
        return sensor_output

    def floor_sensor_calculator_from_coordinates(self, coordinate_array):
        """
        Assuming the output of the floor sensor with linear interpolation for a single floor_sensor.
        """
        inner_radius = self.floor_parameters["inner_radius_of_floor"]
        outer_radius = self.floor_parameters["outer_radius_of_floor"]
        floor_center = [self.floor_parameters["center_x"],
                        self.floor_parameters["center_y"]]
        distance = euclidian_dist(coordinate_array, floor_center)
        if distance > outer_radius:
            return 0
        elif distance < inner_radius:
            return 255
        else:  # y = y0+(x-x0)*(y1-y0)/(x1-x0)
            return int(255 * (1+((inner_radius-distance)/(outer_radius-inner_radius))))

    def update_landmark_sensor(self):
        if self.landmark_parameters["enabled"]:
            self.landmark_sensor_result = self.landmark_sensor_calculator()

    def landmark_sensor_calculator(self):
        """
        Calculate landmark sensor information in z linear and y angular displacement.
        More information in:
        http://wiki.ros.org/tf/Overview/Transformations#tf2.2FTerminology.Frame_Poses for transformation order
        https://www.ros.org/reps/rep-0103.html#suffix-frames for camera link conventions
        """

        # skip if no landmarks assigned to arena
        if not self.landmark_parameters["landmark_coordinates"]:
            return False, 0, 0.0, np.array([0.0,0.0])
        
        # initialize landmark sensor
        max_distance = self.landmark_parameters["max_dist"]
        fov = self.landmark_parameters["fov"]
        landmark_sensor_coordinates = np.zeros(2)
        # NOTE: fixed sensor coordinate calculations, used to be always at 45 degrees, now it properly uses heading info
        sensor_angle = self.pose[2] + np.deg2rad(self.landmark_parameters["sensor_angle_offset"])
        landmark_sensor_coordinates[0] = self.pose[0]*1000 + np.cos(sensor_angle)*self.radius 
        landmark_sensor_coordinates[1] = self.pose[1]*1000 + np.sin(sensor_angle)*self.radius           
        
        # get both the displacement vectors and distances of landmarks to the landmark sensor
        landmark_distances = self.landmark_distance_helper(landmark_sensor_coordinates)
        ratio = self.landmark_parameters["landmark_distance_remap"]

        # NOTE: switching to true tvec form, i.e. displacements in [x,y], 
        # but keeping Euclidean distances as well
        # as we need to use them for thresholding
        landmark_displacements = np.zeros((len(self.landmark_parameters["landmark_coordinates"]),2))
        for i in range(len(self.landmark_parameters["landmark_coordinates"])):
            landmark_displacements[i] = np.asarray(self.landmark_parameters["landmark_coordinates"][i]) - landmark_sensor_coordinates

        # sort landmarks ranked from closest to sensor to farthest, save sorted indices as a list
        sorted_landmark_ids = landmark_distances.argsort()

        for i in range(sorted_landmark_ids.size):
            id = sorted_landmark_ids[i]
            distance_of_landmark_to_sensor = landmark_distances[id]
            if distance_of_landmark_to_sensor > max_distance: # closest landmark not within range
                return False, 0, 0.0, np.array([0.0,0.0])

            # default heading angle at which the landmark is front and center
            default_viewing_orientation = np.deg2rad(self.landmark_parameters["landmark_orientations"][id])
            # the rotation should be applied in rvec such that the primary normal of the landmark is in z. if it's not, apply compensation through this variable
            default_view_offset = self.landmark_parameters["default_view_offset"] 

            # the bearing angle is necessary to check the FOV of the robot
            camera_bearing_angle = np.arctan2(landmark_displacements[id][1], landmark_displacements[id][0]) 
            # updated version -- the orientation is actually independent of the bearing calculation. It is typically received through the AruCo camera calls, which depend on marker orientations directly.
            camera_view_angle = - sensor_angle + default_viewing_orientation + default_view_offset

            # this offset is roughly 11.5 degrees off when the radius is 50 millimeters, a better version can be implemented later?
            if  -fov / 360 * np.pi + sensor_angle < camera_bearing_angle < fov / 360 * np.pi + sensor_angle:
                #return True, angle, distance_of_landmark_to_sensor, sorted_landmark_ids[i]
                return True, id, camera_view_angle, distance_of_landmark_to_sensor*ratio #, viewing_offset # comment the last one out after debugging

    def landmark_distance_helper(self, landmark_sensor_coordinates):
        distances = np.zeros(len(self.landmark_parameters["landmark_coordinates"]))
        for i in range(len(self.landmark_parameters["landmark_coordinates"])):
            distances[i] = euclidian_dist(self.landmark_parameters["landmark_coordinates"][i], landmark_sensor_coordinates)
        return distances

    def simulate_fov(self, obstacles, drones, sensor_direction, max_ang, res):
        # Convert degs to rads
        max_ang = max_ang/180.0*np.pi
        res = res/180.0*np.pi
        # Convert to mm all rays start from same point
        # robot's center
        x = self.pose[0] * 1000.0
        y = self.pose[1] * 1000.0
        min_range = 1300
        is_robot = False
        # angles of the rays discretizing the sensor FoV
        # offset rays by the sensor's direction
        angs = np.arange(-max_ang,max_ang,res) + sensor_direction
        for ang in angs:

            # full line segment of the sensor's ray
            x_end = x + (np.cos(ang) * self.sensor_parameters["max_dist"]) 
            y_end = y + (np.sin(ang) * self.sensor_parameters["max_dist"])

            s_line = LineString([(x, y), (x_end, y_end)])

            range = self.obstacle_detection(s_line, obstacles)
            # range will be updated if a robot is closer than an obstacle
            # range, tmp_is_robot = self.robot_detection(s_line, range, drones)
            if range < min_range: # a closer object detected
                min_range = range
            # if not is_robot and tmp_is_robot:
            #     is_robot = True

        return min_range, is_robot

def wrap2pi(ang):
    """
    Returns given angle in 
    [-pi, +pi] 
    """
    ang = ang % (2*np.pi)
    if ang > np.pi:
        ang = np.pi - ang
        return -(np.pi + ang)
    else:
        return ang

def sensor_mapping(ang):
    num_sensors = 12
    # abgle seperation bw consecutive sensors
    angle_step = 2*np.pi/num_sensors
    # offset for compensating the first sensor being
    # symmetric w.r.t. 0 degree
    offset_angle = angle_step/2
    if ang<0:
        tmp_ang = ang - offset_angle
    else:
        tmp_ang = ang + offset_angle
    signed_sensor_indx = int(tmp_ang/angle_step)
    misalignment = abs((ang/angle_step-signed_sensor_indx)*angle_step)
    # index in [0,11]
    sensor_indx = signed_sensor_indx%num_sensors
    return sensor_indx, misalignment

def unique(list1): 
    """
    Removes the duplicate elements and
    returns the list with unique elements
    """
    unique_list = [] 
      
    for x in list1: 
        if x not in unique_list: 
            unique_list.append(x) 
    return unique_list

# def angle_between_vectors(v1, v2):
#     v1 = v1/np.linalg.norm(v1)
#     v2 = v2/np.linalg.norm(v2)
#     dot_product = np.dot(v1,v2)
#     return np.arccos(-dot_product) #NOTE: Need to negate the dot_product result to bring the FOV in front of the robot, otherwise it's behind it

def angle_between_vectors(v1, v2):
    angle = - np.arctan2(v2[1], v2[0]) + np.arctan2(v1[1], v1[0])
    return angle % 2*np.pi

def equivalence_relation(positions, d_thresh):
    """
    Returns equivalence lists based on 
    the equivelence relation
    """
    list_1 = []
    list_2 = []
    # compare positions with each other
    for i, p_1 in enumerate(positions):
        for j, p_2 in enumerate(positions):
            # euclidian dist is our equivalence
            # relation
            d = euclidian_dist(p_1, p_2)
            if i == j or d == 0:
                # no need to check
                # for equivalence
                continue
            if d < d_thresh:
                # equivalence relation 
                # satisfied
                list_1.append(i)
                list_2.append(j)
    return list_1, list_2

def equivalence_class(list_1, list_2, n, m):
    """
    Computes equivalence classes of KOBOTs
    based on the two input lists defining equivalence 
    relations where m is the size of lists and
    n is number of KOBOTs
    """
    # initialize each element on its own class
    nf = [i for i in range(n)]
    for i in range(m):
        # track first element up to its ancestor
        indx_1 = list_1[i]
        while indx_1 != nf[indx_1]:
            indx_1 = nf[indx_1]
        # track second element up to its ancestor
        indx_2 = list_2[i]
        while indx_2 != nf[indx_2]:
            indx_2 = nf[indx_2]
        if indx_1 != indx_2:
            # if they are not related
            # make them related
            nf[indx_1] = indx_2
    # final sweep up to highest ancestors
    for i in range(n):
        while nf[i] != nf[nf[i]]:
            nf[i] = nf[nf[i]]
    return nf

def euclidian_dist(p_1, p_2):
    """
    Returns the euclidian dist.
    """
    diff_x = p_1[0] - p_2[0]
    diff_y = p_1[1] - p_2[1]
    return np.linalg.norm([diff_y, diff_x])

    
