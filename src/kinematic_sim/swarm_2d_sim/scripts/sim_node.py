#!/usr/bin/env python3

import rospy
from simulator import Simulator

sim = None

if __name__ == '__main__':
    rospy.init_node('sim_node')
    rospy.loginfo('Initializing Simulation Node')
    sim = Simulator()

    sim.env.init_render_frame()

    while not rospy.is_shutdown():
        sim.sim_loop()